import Routing from './src/navigation/Routing'
import React from 'react';
import { SafeAreaView } from 'react-native';

const App = () => {
  return(

        <SafeAreaView style={{flex:1}}>
          <Routing/>
        </SafeAreaView>

  )
}

export default App;