import React from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native'
import { InputText, TextBold, TextRegular } from '../../global'
import { Colors } from '../../../styles'

const LoginComponent = ({
    navigation,
    email,
    setEmail,
    password,
    setPassword,
    hidePassword,
    setShowPassword,
    onPressLogin,
    SEARCH_HEIGHT,
    SEARCH_WIDTH,
}) => {

    return (
        <View style={{ flex: 1, backgroundColor: '#F7F7F7' }}>
            <View style={{
                height: SEARCH_HEIGHT,
                width: SEARCH_WIDTH,
                paddingHorizontal: 20,
                justifyContent: 'center',
            }}>
                <View style={{
                    backgroundColor: Colors.WHITE,
                    borderRadius: 10,
                    paddingVertical: 16,
                }}>
                    <Text style={{
                        paddingHorizontal: 20,
                        paddingVertical: 8,
                        backgroundColor: '#002558',
                        color: Colors.WHITE,
                        textAlign: 'center',
                        borderRadius: 10,
                        alignSelf: 'center',
                        top:'-10%',
                    }}>Digital Approval</Text>
                    <TextBold
                        text="Login"
                        size={20}
                        color={Colors.BLACK}
                        style={{ alignSelf: 'center' }}
                    />
                    <InputText
                        placeholderText='Masukkan Email'
                        style={styles.inputText}
                        onChangeText={(text) => setEmail(text)}
                        value={email}
                    />
                    <InputText
                        placeholderText='Masukkan Password'
                        style={styles.inputText}
                        onChangeText={(text) => setPassword(text)}
                        value={password}
                        isPassword={true}
                        hidePassword={hidePassword}
                        setShowPassword={setShowPassword}
                    />
                    <TouchableOpacity
                        style={styles.btnLogin}
                        onPress={onPressLogin}
                    >
                        <TextRegular
                            text="Log In"
                            color={Colors.WHITE}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    inputText: {
        marginTop: 20,
        width: '90%',
        alignSelf: 'center'
    },
    btnLogin: {
        width: '90%',
        alignSelf: 'center',
        marginTop: 20,
        paddingVertical: 10,
        borderRadius: 6,
        backgroundColor: Colors.PRIMARY,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default LoginComponent