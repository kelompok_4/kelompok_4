import React, { useState } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'
import { TextRegular, TextBold, TextMedium, Header, InputText } from '../../global'
import { Colors } from '../../../styles'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import IconF from 'react-native-vector-icons/FontAwesome'
import ModalBottom from '../../modal/ModalBottom'
import { TextInput } from 'react-native-gesture-handler'
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const RiwayatComponent = ({
    navigation,
    modalBottom,
    setModalBottom,
    newCar,
    setNewCar,
    search,
    setSearch,
    SEARCH_WIDTH
}) => {

    const [searching, setSearching] = useState('')

    const [modalTracking, setModalTracking] = useState('')

    const [available, setAvailable] = useState('');
    const handleOpen = () => {
        setAvailable('');
    };
    const handleClose = () => {
        setAvailable('Close');
    };


    return (
        <View>
            <View style={{ backgroundColor: '#287AE5', height: 135, padding: 15 }}>
                <TextBold
                    text="Riwayat"
                    color={Colors.WHITE}
                    size={20}
                    style={{ marginBottom: 15 }}
                />

                <View style={{
                    flexDirection: 'row',
                    height: 48,
                    width: SEARCH_WIDTH,
                }}>
                    <View style={{
                        backgroundColor: '#FFFFFF',
                        height: 48,
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'space-between',
                        borderRadius: 8,
                        paddingHorizontal:5
                    }}>
                        <TextInput placeholder='Telusuri....'
                            onChangeText={(text) => setSearching(text)}
                            value={searching}></TextInput>
                        <TouchableOpacity onPress={() => ''}
                            style={{ alignSelf: 'center' }}>
                            <AntDesign name="search1" size={30} color="black" />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => setModalBottom(true)}>
                        <MaterialCommunityIcons name="tune" size={25} color="black"
                            style={{
                                backgroundColor: '#FFFFFF',
                                borderRadius: 8,
                                padding: 12,
                                marginLeft: 12,
                            }} />
                    </TouchableOpacity>
                </View>

                <View style={{
                    backgroundColor: '#FFFFFF',
                    paddingHorizontal: 12,
                    borderRadius: 8,
                    marginVertical: '4%',
                    flexDirection: 'row',
                    alignSelf: 'center',
                }}>
                    <TouchableOpacity onPress={() => handleOpen()}
                        style={styles.buttonTrade}>
                        {available === '' ?
                            <Text style={{ color: '#287AE5' }}>Trade In</Text>
                            :
                            available === 'Close' ?
                                <Text style={{ color: 'grey' }}>Trade In</Text>
                                : ""}
                    </TouchableOpacity>
                    <View style={{
                        borderColor: 'rgba(0,0,0,0.1)',
                        borderWidth: 1,
                        height: '100%',

                    }} />
                    <TouchableOpacity onPress={() => handleClose()}
                        style={styles.buttonTrade}>
                        {available === '' ?
                            <Text style={{ color: 'grey' }}>New Car</Text>
                            :
                            available === 'Close' ?
                                <Text style={{ color: '#287AE5' }}>New Car</Text>
                                : ""}
                    </TouchableOpacity>
                </View>


                {available ?
                    <TouchableOpacity onPress={() => navigation.navigate("SummaryNewCar")}
                        style={{ backgroundColor: 'white', borderRadius: 8, padding: 10, marginBottom: 10, borderWidth: 1, borderColor: '#CCD3DD' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ color: 'grey', fontSize: 12 }}>TR-092018-246</Text>
                            <Text style={{ color: '#002558', fontSize: 12 }}>Sen, 17 Sep 2018 - 10:30</Text>
                        </View>
                        <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold', marginVertical: 8 }}>Avanza G 2.0</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderTopWidth: 1, borderColor: '#ccc', borderStyle: 'dashed', paddingTop: 10 }}>
                            <View>
                                <Text style={{ color: '#002558', fontSize: 12, fontWeight: 'bold' }}>Handoko</Text>
                                <Text style={{ color: 'grey', fontSize: 10 }}>Salesman</Text>
                            </View>
                            <View style={{ backgroundColor: '#FF6127', borderRadius: 4, paddingHorizontal: 5, justifyContent: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 10 }}>No Deal</Text>
                            </View>
                        </View>
                    </TouchableOpacity>



                    :

                    <View>

                        <TouchableOpacity onPress={() => navigation.navigate("SummaryTradeIn")}
                            style={{ backgroundColor: 'white', borderRadius: 8, padding: 10, marginBottom: 10, borderWidth: 1, borderColor: '#CCD3DD' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: 'grey', fontSize: 12 }}>TR-092018-246</Text>
                                <Text style={{ color: '#002558', fontSize: 12 }}>Sen, 17 Sep 2018 - 10:30</Text>
                            </View>
                            <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold', marginVertical: 8 }}>Avanza G 2.0</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderTopWidth: 1, borderColor: '#ccc', borderStyle: 'dashed', paddingTop: 10 }}>
                                <View>
                                    <Text style={{ color: '#002558', fontSize: 12, fontWeight: 'bold' }}>Handoko</Text>
                                    <Text style={{ color: 'grey', fontSize: 10 }}>Salesman</Text>
                                </View>
                                <View style={{ backgroundColor: '#FF6127', borderRadius: 4, paddingHorizontal: 5, justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 10 }}>No Deal</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate("SummaryTradeIn")}
                            style={{ backgroundColor: 'white', borderRadius: 8, padding: 10, marginBottom: 10, borderWidth: 1, borderColor: '#CCD3DD' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: 'grey', fontSize: 12 }}>TR-092018-246</Text>
                                <Text style={{ color: '#002558', fontSize: 12 }}>Sen, 17 Sep 2018 - 10:30</Text>
                            </View>
                            <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold', marginVertical: 8 }}>Avanza G 2.0</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderTopWidth: 1, borderColor: '#ccc', borderStyle: 'dashed', paddingTop: 10 }}>
                                <View>
                                    <Text style={{ color: '#002558', fontSize: 12, fontWeight: 'bold' }}>Handoko</Text>
                                    <Text style={{ color: 'grey', fontSize: 10 }}>Salesman</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ backgroundColor: '#A4D166', borderRadius: 4, paddingHorizontal: 5, justifyContent: 'center' }}>
                                        <Text style={{ color: 'white', fontSize: 10 }}>Deal</Text>
                                    </View>
                                    <Text style={{ justifyContent: 'center', fontSize: 20, color: '#A4D166', marginLeft: 10 }}>✓</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                    </View>
                }

            </View>

            <ModalBottom
                show={modalBottom}
                onClose={() => setModalBottom(false)}
                title='Filter'
            >
                <View style={{ padding: 10 }}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, borderBottomWidth: 0.5, borderBottomColor: '#ccc' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>Deal</Text>
                        <IconF name="circle-o" size={16} color="#002558" />
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>No Deal</Text>
                        <IconF name="dot-circle-o" size={16} color="#002558" />
                    </View>

                </View>
                <View style={{ width: '100%', padding: 10 }}>
                    <Text style={{
                        backgroundColor: '#287AE5',
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: 'white',
                        textAlign: 'center',
                        padding: 5,
                        borderRadius: 8
                    }}>SIMPAN</Text>
                </View>
            </ModalBottom>

        </View>
    )
}

const styles = StyleSheet.create({
    buttonTrade: {
        paddingVertical: 9,
        width: '50%',
        alignItems: 'center',
        borderColor: '#0000000F',
    },
    idNewCar: {
        fontSize: 12,
        textAlign: 'left',
        color: 'rgba(0, 37, 88, 0.5)',
    },
    tanggal: {
        fontSize: 12,
        color: '#002558',
        right: 0,
        position: 'absolute'
    },
    namaMobil: {
        fontSize: 14,
        marginTop: 8,
        color: '#002558',
        fontWeight: 'bold'
    },
    garisPembatas: {
        marginTop: 10,
        borderStyle: 'dashed',
        borderWidth: 1,
        borderRadius: 1,
        borderColor: 'rgba(0,0,0,0.3)',
    },
    namaPekerja: {
        fontSize: 12,
        color: '#002558',
        fontWeight: 'bold',
    },
    pekerjaan: {
        fontSize: 10,
        color: 'rgba(0, 37, 88, 0.5)',
    },
    dalamRequest: {
        backgroundColor: '#002558',
        textAlign: 'right',
        color: '#FFFFFF',
        fontSize: 10,
        borderRadius: 10,
        padding: 5,
        alignSelf: 'center'
    },
    sudahRequest: {
        backgroundColor: '#FF6127',
        textAlign: 'right',
        color: '#FFFFFF',
        fontSize: 10,
        borderRadius: 10,
        padding: 5,
        alignSelf: 'center'
    }

})


export default RiwayatComponent;
