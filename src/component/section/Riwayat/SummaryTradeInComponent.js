import React, { useState } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native'
import { TextRegular, TextBold, TextMedium, Header, InputText } from '../../global'
import Icon from 'react-native-vector-icons/FontAwesome'
import ModalCenter from '../../modal/ModalCenter'

const SummaryTradeInComponent = ({
    navigation,
    route,
    deal,
    setDeal,
    modalOdoo,
    setModalOdoo,
    modalError,
    setModalError
}) => {

    return (
        <View style={{ flex: 1 }}><ScrollView>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', padding: 15 }}>
                <Text style={{ color: 'grey', fontSize: 12 }}>TR-092018-246</Text>
                <Text style={{ color: '#002558', fontSize: 12 }}>Sen, 17 Sep 2018 - 10:30</Text>
            </View>

            <View style={{ margin: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold', paddingBottom: 15, borderBottomWidth: 0.5, borderBottomColor: '#ccc' }}>Used Car Information</Text>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Mobil</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>AVANZA G AT 2016</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Plat Nomor</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>B 1234 TES</Text>
                </View>
                <TouchableOpacity onPress={()=>navigation.navigate("DetailAppraisal")} style={{ backgroundColor: '#287AE5', borderRadius: 4, padding: 10, marginTop: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: "white" }}>Lihat Detail Appraisal</Text>
                    <Icon name="arrow-right" color="white" />
                </TouchableOpacity>
            </View>

            <View style={{ marginHorizontal: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold', paddingBottom: 15, borderBottomWidth: 0.5, borderBottomColor: '#ccc' }}>New Car Information</Text>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Mobil</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rush S AT TRD</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>OTR</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 276.600.000</Text>
                </View>
            </View>

            <View style={{ margin: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <View style={{ flexDirection: 'row', paddingBottom: 15, borderBottomWidth: 0.5, borderBottomColor: '#ccc', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Ruang Nego Harga</Text>
                    <Text>terupdate*</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Harga Penawaran</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 78.500.000</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Ruang Nego</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 3.000.000</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>MRP (Harga Max)</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 81.500.000</Text>
                </View>
            </View>

            <View style={{ marginHorizontal: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <View style={{ flexDirection: 'row', paddingBottom: 15, borderBottomWidth: 0.5, borderBottomColor: '#ccc', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Diskon</Text>
                    <Text>terupdate*</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Diskon Kacab</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 2.000.000</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Pengajuan Diskon BOD</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>-</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Proyeksi GP (After Disc)</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>-</Text>
                </View>
            </View>

            <View style={{ margin: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <View style={{ flexDirection: 'row', paddingBottom: 15, borderBottomWidth: 0.5, borderBottomColor: '#ccc', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Gross Profit AAT</Text>
                    <Text>terupdate*</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Proyeksi Harga Jual</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 120.000.000</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>COGS (harga beli + biaya rekondisi) + OPEX</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 109.550.000</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                    <Text style={{ color: '#002558', fontSize: 14 }}>Proyeksi NPBT AAT</Text>
                    <Text style={{ color: '#002558', fontSize: 14, fontWeight: 'bold' }}>Rp 10.450.000</Text>
                </View>
            </View>

            <View style={{ marginHorizontal: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <Text style={{ color: '#002558', fontSize: 12 }}>Harga Final</Text>
                <Text style={{ borderColor: '#E5E8EE', borderWidth: 0.5, borderRadius: 4, color: '#002558', fontSize: 14, padding: 10, marginVertical: 5 }}>Rp 170.000.000</Text>
                <Text style={{ color: '#002558', fontSize: 12, marginTop: 10 }}>Sisa Ruang Nego</Text>
                <Text style={{ borderColor: '#E5E8EE', borderWidth: 0.5, borderRadius: 4, color: '#002558', fontSize: 14, padding: 10, marginVertical: 5 }}>Rp 2.000.000</Text>
                <Text style={{ color: '#002558', fontSize: 12, marginTop: 10 }}>Total Benefit Trade-in (Final)</Text>
                <Text style={{ borderColor: '#E5E8EE', borderWidth: 0.5, borderRadius: 4, color: '#002558', fontSize: 14, padding: 10, marginVertical: 5 }}>Rp 2.000.000</Text>
            </View>

            <Text style={{ color: '#002558', fontSize: 16, fontWeight: 'bold', textAlign: 'center', paddingVertical: 15, borderBottomColor: '#ccc', borderBottomWidth: 0.5 }}>History Log</Text>

            <View style={{ height: 36, backgroundColor: 'white', margin: 15, borderRadius: 8, flexDirection: 'row' }}>
                <TouchableOpacity onPress={() => setDeal(true)} style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                    {deal ?
                        <TextBold
                            text='Deal'
                            color='#287AE5'
                        />
                        :
                        <Text>Deal</Text>
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setDeal(false)} style={{ width: '50%', justifyContent: 'center', alignItems: 'center' }}>
                    {deal ?
                        <Text>Request</Text>
                        :
                        <TextBold
                            text='Request'
                            color='#287AE5'
                        />
                    }
                </TouchableOpacity>
            </View>

            <View style={{ margin: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <Text style={{ color: '#A4D166', fontSize: 12, fontWeight: 'bold' }}>Deal by OM</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 3 }}>
                    <Text style={{ color: '#002558', fontSize: 10, marginRight: 5, paddingRight: 5, borderRightColor: '#ccc', borderRightWidth: 2 }}>Sen, 17 Sep 2018 - 10:30</Text>
                    <Text style={{ color: '#002558', fontSize: 10, marginRight: 5, paddingRight: 5, borderRightColor: '#ccc', borderRightWidth: 2 }}>Helmi Candra</Text>
                    <Text style={{ color: '#002558', fontSize: 10 }}>Rp 99.000.000</Text>
                </View>
            </View>

            <View style={{ margin: 15, backgroundColor: 'white', padding: 10, borderRadius: 4 }}>
                <Text style={{ color: '#FF6127', fontSize: 12, fontWeight: 'bold' }}>NO DEAL</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 3 }}>
                    <Text style={{ color: '#002558', fontSize: 10, marginRight: 5, paddingRight: 5, borderRightColor: '#ccc', borderRightWidth: 2 }}>Sen, 17 Sep 2018 - 10:30</Text>
                    <Text style={{ color: '#002558', fontSize: 10, marginRight: 5, paddingRight: 5, borderRightColor: '#ccc', borderRightWidth: 2 }}>Helmi Candra</Text>
                    <Text style={{ color: '#002558', fontSize: 10 }}>Rp 99.000.000</Text>
                </View>
            </View>

            <TouchableOpacity onPress={() => setModalOdoo(true)} style={{ backgroundColor: '#287AE5', borderRadius: 4, padding: 10, margin: 15, justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ color: "white", fontSize: 14 }}>TERUSKAN KE ODOO</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => setModalError(true)} style={{ backgroundColor: 'red', borderRadius: 4, padding: 10, margin: 15, justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ color: "white", fontSize: 14 }}>TEST MODAL ERROR</Text>
            </TouchableOpacity>


            <ModalCenter show={modalOdoo} title="Konfirmasi Odoo" onClose={() => setModalOdoo(false)}>
                <View style={{ padding: 10 }}>

                    <Text style={{ fontSize: 16, color: '#002558' }}>Apakah Anda yakin akan meneruskan ke Odoo?</Text>
                    <TouchableOpacity style={{ backgroundColor: '#287AE5', borderRadius: 4, padding: 10, marginVertical: 25, alignItems: 'center' }}>
                        <Text style={{ color: "white", fontSize: 14 }}>YA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center', padding: 10 }} onPress={() => setModalOdoo(false)}>
                        <Text style={{ color: "grey", fontSize: 14 }}>BATAL</Text>
                    </TouchableOpacity>

                </View>
            </ModalCenter>


            <ModalCenter show={modalError} title="Error!" onClose={() => setModalError(false)}>
                <View style={{ padding: 10, alignItems: 'center' }}>

                    <View style={{ width: 75, height: 75, borderRadius: 50, alignContent: 'center', alignItems: 'center', backgroundColor: 'red', margin: 25 }}>
                        <Icon name="close" size={70} style={{ color: 'white' }} />
                    </View>

                    <Text style={{ fontSize: 20, color: '#002558' }}>Data Kurang Lengkap</Text>
                    <Text style={{ fontSize: 16, color: '#002558' }}>Silakan cek ulang data yang belum lengkap</Text>
                    <TouchableOpacity style={{ alignSelf: 'flex-end', padding: 10 }} onPress={() => setModalError(false)}>
                        <Text style={{ color: "#287AE5", fontSize: 14 }}>KEMBALI</Text>
                    </TouchableOpacity>

                </View>
            </ModalCenter>

        </ScrollView></View>
    )
}


export default SummaryTradeInComponent;