import React, { useState } from 'react'
import {
    View,
    Text,
    ScrollView,
    StyleSheet
} from 'react-native'

const SummaryTradeInComponent = ({
    navigation,
    route,
}) => {

    return (
        <View style={{ flex: 1 }}>
            <ScrollView>
                <View style={styles.headerView}>
                    <Text style={styles.headerText}>Deal</Text>
                    <Text style={styles.headerText}>19 Agutus 2020 - 15.43</Text>
                </View>
                <View style={styles.coloumView}>
                    <Text style={{ color: 'rgba(0, 0, 0, 0.3)', fontSize: 12 }}>TR-092018-246</Text>
                    <Text style={styles.text}>Kepala Cabang Bintaro</Text>
                </View>

                <View>
                    <View style={styles.coloumView2}>
                        <Text style={styles.text}>Nama Customer</Text>
                        <Text style={styles.textBold}>NANDO DWIKI SATRIA</Text>
                    </View>

                    <View style={styles.coloumView2}>
                        <Text style={styles.text}>No.Hp</Text>
                        <Text style={styles.text}>082245884655</Text>
                    </View>
                </View>

                <View style={styles.coloumViewMultiple}>
                    <View style={styles.coloumView2}>
                        <Text style={styles.textBold}>Detail Mobil</Text>
                    </View>
                    <View style={styles.underLine} />

                    <View style={styles.viewTitle}>
                    <Text style={styles.title}>Used Car</Text>
                    </View>

                    <View style={styles.coloumView2}>
                        <Text style={styles.textBold}>Rush S AT TRD</Text>
                        <Text style={styles.textBold}>Rp 276.600.000</Text>
                    </View>
                </View>


                <View style={styles.coloumViewMultiple}>
                    <View style={styles.coloumView2}>
                        <Text style={styles.textBold}>Request Diskon</Text>
                    </View>
                    <View style={styles.underLine} />

                    <View style={styles.coloumView2}>
                        <Text style={styles.text}>Diskon</Text>
                        <Text style={styles.textBold}>Rp 15.000.000</Text>
                    </View>
                    <View style={styles.coloumView2}>
                        <Text style={styles.text}>Harga Mobil</Text>
                        <Text style={styles.textBold}>Rp 235.000.000</Text>
                    </View>
                </View>


            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#A4D166',
        height: 36,
        alignItems: 'center',
        paddingHorizontal: 16
    },
    headerText: {
        color: '#FFFFFF',
        fontSize: 12
    },
    coloumView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        padding: 15,
        borderColor: 'rgba(0, 37, 88, 0.1)',
        borderBottomWidth: 0.6,
    },
    coloumViewMultiple: {
        marginTop:8
    },
    viewTitle: {
        backgroundColor:'white',
        paddingHorizontal:15,
        paddingTop:15,
    },
    coloumView2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        padding: 15,
    },
    
    text: {
        color: '#002558',
        fontSize: 12
    },
    textBold: {
        color: '#002558',
        fontSize: 12,
        fontWeight: 'bold',
    },
    underLine: {
        borderWidth: 0.3,
        borderColor: 'rgba(0, 37, 88, 0.1)',
        borderStyle: 'solid',
    },
    title:{
        color: 'rgba(0,0,0,0.5)',
        fontSize: 12,
        
    },
})

export default SummaryTradeInComponent;