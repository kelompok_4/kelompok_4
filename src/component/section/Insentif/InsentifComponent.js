import React, {useState} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
 } from 'react-native'
 import {TextRegular, TextBold, TextMedium, Header, InputText} from '../../global'
 import { Colors } from '../../../styles'
 import Icon from 'react-native-vector-icons/FontAwesome'
 import ModalBottom from '../../modal/ModalBottom'

const InsentifComponent = ({
}) => {

   return(
       <View>
        <View style={{backgroundColor:'#287AE5', padding:15}}>
            <TextBold
            text="Insentif"
            color={Colors.WHITE}
            size={20}
            />
        </View>

<View style={{padding:15, flexDirection:'row', justifyContent:'space-between'}}>
    <View>
    <Text style={{fontWeight:'bold', fontSize:20, color:'#002558'}}>June 2023</Text>
    <Text style={{fontSize:14, color:'#002558'}}>Total Insentif    <Text style={{fontWeight:'bold'}}>Rp 0</Text></Text>
    </View>
    <TouchableOpacity>
    <Icon name="calendar" size={24} color="#002558"/>
    </TouchableOpacity>
</View>

<View style={{marginHorizontal:15, marginBottom:15, padding:10, backgroundColor:'white', borderWidth:0.5, borderColor:'#CCD3DD', borderRadius:8}}>
    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
    <Text style={{color:'#002558', fontSize:12}}>Target Bulanan</Text>
    <Text style={{color:'#002558', fontSize:12, fontWeight:'bold'}}>0%</Text>
    </View>
    <View style={{height:12, backgroundColor:'#F7F7F7', borderRadius:6, marginVertical:10}}></View>
    <Text style={{color:'grey', textAlign:'right', fontSize:12}}>8/10 Unit PO Valid</Text>
</View>

<Text style={{color:'#ccc', textAlign:'center', fontSize:16}}>No Data</Text>
       </View>
   )
}

export default InsentifComponent;
