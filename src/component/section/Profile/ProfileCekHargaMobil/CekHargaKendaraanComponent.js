import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { TextRegular, TextBold, TextMedium, Header, InputText } from '../global';
import Icon from 'react-native-vector-icons/AntDesign';
import { ScrollView } from 'react-native-gesture-handler';

const CekHargaKendaraanComponent = ({ navigation }) => {
  const SEARCH_WIDTH = Dimensions.get('window').width
  const SEARCH_HEIGHT = Dimensions.get('window').height
  const [show, setShow] = useState(null);

  return (
    <View style={{ flex: 1, backgroundColor: '#f7f7f7', width: SEARCH_WIDTH, height: SEARCH_HEIGHT }}>
      <View style={{ width: '100%', height: '100%' }}>
        <View style={styles.container}>
          <ScrollView>
            <Text style={styles.text}>Brand</Text>
            <TouchableOpacity onPress={() => setShow(show === 1 ? null : 1)}
              style={styles.view}>
              <Text style={styles.text2}>Toyota</Text>
              <Icon name={show === 1 ? 'up' : 'down'} size={17} color={'black'} />
            </TouchableOpacity>
            {show === 1 ? (
              <View style={styles.view2}>
                <TouchableOpacity>
                  <Text style={styles.text3}>Toyota</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <Text style={styles.text}>Model</Text>
            <TouchableOpacity onPress={() => setShow(show === 3 ? null : 3)}
              style={styles.view}>
              <Text style={styles.text2}>Camry</Text>
              <Icon name={show === 3 ? 'up' : 'down'} size={17} color={'black'} />
            </TouchableOpacity>
            {show === 3 ? (
              <View style={styles.view2}>
                <TouchableOpacity>
                  <Text style={styles.text3}>Camry</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <Text style={styles.text}>Tipe</Text>
            <TouchableOpacity onPress={() => setShow(show === 4 ? null : 4)}
              style={styles.view}>
              <Text style={styles.text2}>All New Camry 3.5 Q AT</Text>
              <Icon name={show === 4 ? 'up' : 'down'} size={17} color={'black'} />
            </TouchableOpacity>
            {show === 4 ? (
              <View style={styles.view2}>
                <TouchableOpacity>
                  <Text style={styles.text3}>All New Camry 3.5 Q AT</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <Text style={styles.text}>Tahun</Text>
            <TouchableOpacity onPress={() => setShow(show === 5 ? null : 5)}
              style={styles.view}>
              <Text style={styles.text2}>2017</Text>
              <Icon name={show === 5 ? 'up' : 'down'} size={17} color={'black'} />
            </TouchableOpacity>
            {show === 5 ? (
              <View style={styles.view2}>
                <TouchableOpacity>
                  <Text style={styles.text3}>2017</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <View style={styles.view3}>
              <Text style={styles.text}>Kuota cek harga hari ini:</Text>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.text2}>0</Text>
                <Text style={styles.text2}>/50</Text>
              </View>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate("CekHasilHarga")}
            >
              <Text style={styles.button}>CEK HARGA</Text>
            </TouchableOpacity>
            <TouchableOpacity >
              <Text style={styles.button2}>MOBIL TIDAK ADA DI LIST</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    </View >
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 12,
    margin: 12,
  },
  view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#E5E8EE',
    paddingVertical: 5,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginTop: 4,
  },
  view2: {
    backgroundColor: '#fff',
    // elevation: 9,
    borderColor: 'rgba(0,0,0,0.2)',
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 12,
    marginBottom: 10,
  },
  view3: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    marginTop: 5,
  },
  text2: {
    fontWeight: 'bold',
    textAlignVertical: 'center',
  },
  text3: {
    fontWeight: '300',
  },
  button: {
    marginTop: 23,
    backgroundColor: '#287AE5',
    borderRadius: 8,
    paddingVertical: 8,
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
  button2: {
    marginTop: 23,
    borderColor: '#287AE5',
    backgroundColor: 'white',
    borderWidth: 2,
    borderRadius: 8,
    paddingVertical: 8,
    textAlign: 'center',
    color: '#287AE5',
    fontWeight: 'bold',
  },
})

export default CekHargaKendaraanComponent;
