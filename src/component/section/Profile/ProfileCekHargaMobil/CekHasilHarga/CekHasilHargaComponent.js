import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Dimensions,
} from 'react-native';


const CekHasilHargaComponent = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width
    const SEARCH_HEIGHT = Dimensions.get('window').height
    const [show, setShow] = useState(null);

    return (
        <View style={{ flex: 1, backgroundColor: 'white', width: SEARCH_WIDTH, height: SEARCH_HEIGHT }}>
            <View style={{ width: '100%', height: '100%' }}>
                <ScrollView style={styles.container}>
                    <Text style={styles.text}>Perkiraan Harga</Text>
                    <Text style={styles.text2}>TOYOTA</Text>
                    <View style={{ flexDirection: 'row', height: 'auto', marginTop: '-3%' }}>
                        <View style={{ width: '20%' }}></View>
                        <View style={{
                            width: 'auto',
                            borderColor: 'rgba( 40,122,229,0.5)',
                            borderWidth: 1,
                            alignItems: 'center',
                            paddingHorizontal: 11
                        }}>
                            <Text style={{ marginTop: 11, color: '#002558', fontWeight: 'bold' }}>Camry</Text>
                            <Text style={{ color: '#002558', marginVertical: 11 }}>All New Camty 3.5 Q AT</Text>
                        </View>
                        <View style={{ width: '20%' }}></View>
                    </View>
                    <View style={{
                        backgroundColor: '#287AE5',
                        borderRadius: 8,
                        padding: 8,
                        width: '100%',
                        height: 'auto',
                        marginTop: 20,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            paddingVertical: 7,
                            backgroundColor: 'rgba(255,255,255,0.2)',
                            color: 'white',
                            width: '100%',
                            textAlign: 'center',
                            borderRadius: 8,
                            fontWeight: 'bold',
                        }}>*Harga berlaku untuk Plat B</Text>
                        <View style={{
                            marginTop: 8,
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            alignItems: 'center'
                        }}>
                            <Text style={{ color: 'white', fontWeight: 'bold', width: '45%', textAlign: 'left' }}>Rp 80.000.000</Text>
                            <View style={{ backgroundColor: 'white', height: 1, width: '10%' }}></View>
                            <Text style={{ color: 'white', fontWeight: 'bold', width: '45%', textAlign: 'right' }}>Rp 80.000.000</Text>
                        </View>
                    </View>
                    <View style={{
                        marginTop: 20,
                        width: '100%',
                        borderColor: '#E5E8EE',
                        flexDirection: 'row',
                        borderWidth: 3,
                        paddingVertical: 13,
                        paddingHorizontal: 6,
                        borderRadius: 8,
                    }}>
                        <View style={{ alignItems: 'center', width: '30%' }}>
                            <Text>Estimasi Kilometer</Text>
                            <Text style={{ color: '#002558', fontWeight: 'bold' }}>{'<'} 180.000</Text>
                        </View>
                        <View style={{ width: 3, height: '100%', backgroundColor: '#E5E8EE', marginHorizontal: 9 }}></View>
                        <View style={{ alignItems: 'center', width: '30%' }}>
                            <Text>Estimasi Warna</Text>
                            <Text style={{ color: '#002558', fontWeight: 'bold' }}>Non Hitam/Putih</Text>
                        </View>
                        <View style={{ width: 3, height: '100%', backgroundColor: '#E5E8EE', marginHorizontal: 9 }}></View>
                        <View style={{ alignItems: 'center', width: '30%' }}>
                            <Text>Estimasi Tahun</Text>
                            <Text style={{ color: '#002558', fontWeight: 'bold' }}>2007</Text>
                        </View>
                    </View>
                    <View style={{
                        marginTop: 20,
                        width: '100%',
                        borderColor: '#E5E8EE',
                        flexDirection: 'row',
                        borderWidth: 3,
                        paddingVertical: 13,
                        paddingHorizontal: 6,
                        borderRadius: 8,
                    }}>
                        <View style={{ alignItems: 'center', width: '100%' }}>
                            <Text>Keterangan</Text>
                            <View style={{ width: '100%', height: 3, backgroundColor: '#E5E8EE', marginVertical: 9 }}></View>
                            <Text>Harga yang diberikan merupakan estimasi awal yang dapat di tawarkan Toyota Trust dan bukan merupakan harga final setelah proses appraisal.</Text>
                        </View>
                    </View>
                    <View style={{marginBottom:20}}></View>
                </ScrollView>
            </View>
        </View >
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 12,
        margin: 12,
        height:'auto',
    },
    text: {
        color: '#002558',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    text2: {
        color: '#FFFFFF',
        backgroundColor: '#287AE5',
        borderRadius: 8,
        textAlign: 'center',
        marginTop: 16,
        paddingHorizontal: 16,
        paddingVertical: 5,
        alignSelf: 'center',
        fontWeight: 'bold'
    },
})

export default CekHasilHargaComponent;

