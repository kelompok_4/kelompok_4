import React, {useEffect, useState} from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Dimensions,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {useSelector, useDispatch} from  'react-redux'
import axios from 'axios';


const ProfileComponent = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width

const dispatch = useDispatch()

const onLogout = async () =>{
    dispatch({type: "LOGOUT"})
    navigation.replace('AuthNavigation')
}
const { isLoggedIn, userData } = useSelector((state) => state.auth);
console.log('cek userData di local storage', userData);


    useEffect(() => {
        getDataUser()
    },[])

    const [dataPengguna, setDataPengguna] = useState({});
    const getDataUser = async () => {
        try {
          const response = await axios.get(
            `http://147.139.209.190:3002/api/v1/user/${userData.user.id}`,
            {
              headers: {
                'Content-Type': 'application/json',
                Authorization: userData.token,
              },
            }
          );
      
          const result = response.data;
          console.log('Success:', result);
      
          if (result.code === 200) {
            console.log('berhasil mengambil data user: ', result);
            setDataPengguna(result)
          }
      
          if (result.code === 400) {
            alert(result.message);
            return false;
          }
        } catch (error) {
          console.error('Error:', error);
          return false;
        }
      };
      

    return (
        <View style={{ flex: 1, backgroundColor: '#f7f7f7' }}>
            <ImageBackground
                source={require('../../../asset/image/Profile/backgroundprofile.png')}
                style={{
                    width: SEARCH_WIDTH,
                    height: 142,
                    alignItems: 'center',
                    paddingHorizontal: 17,
                    paddingVertical: 20
                }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '35%' }} />
                    <View style={{ width: '35%' }}>
                        <View
                            style={styles.container}>
                            <Image
                                source={require('../../../asset/image/Profile/arimakana.jpg')}
                                style={styles.image}
                            />
                        </View>
                    </View>
                    <View style={{ width: '30%', flexDirection: 'row-reverse' }}>
                        <TouchableOpacity onPress={onLogout}>
                            <AntDesign
                                name="logout"
                                size={20}
                                color={'#fff'} />
                        </TouchableOpacity>
                    </View>
                </View>
                <Text
                    style={styles.name}> {userData.user.name}
                    {"\n"}contoh tarik data dari api: {dataPengguna.profile && dataPengguna.profile.areaBranchId.toString()}
                </Text>
            </ImageBackground>
            <View style={{
                marginTop: '5%',
                marginHorizontal: 0,
            }}>
                <TouchableOpacity
                    onPress={() => navigation.navigate("CekHargaKendaraan")}
                    style={{
                        backgroundColor: '#287AE5',
                        borderRadius: 4,
                        paddingVertical: 15,
                        paddingHorizontal: 16,
                        marginHorizontal: 16,
                        fontSize: 14,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <Text style={{ color: 'white' }}>Cek harga Mobil</Text>
                    <FontAwesome name="arrow-right" color="white" />
                </TouchableOpacity>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 10 }}
                    style={{ marginHorizontal: 16 }}>
                    <View style={styles.view}>
                        <View style={styles.view2}>
                            <Text style={styles.text2}
                            >Data Diri
                            </Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('DataDiri')}>
                                <Text style={styles.text3}>
                                    Edit Data Diri
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.line} />
                        <View style={{ paddingHorizontal: 12 }}>
                            <Text
                                style={styles.text}>
                                Nama Supervisor
                            </Text>
                            <Text
                                style={styles.text2}>
                                {userData.user.name}
                            </Text>
                            <Text style={styles.text}>
                                Cabang
                            </Text>
                            <Text
                                style={styles.text2}>
                                Kolektif Colaborations Space
                            </Text>
                            <Text style={styles.text}>
                                No Telepon
                            </Text>
                            <Text
                                style={styles.text2}>
                                081245651200
                            </Text>
                            <Text style={styles.text}>
                                Tanggal Lahir
                            </Text>
                            <Text
                                style={styles.text2}>
                                13-06-2003
                            </Text>
                        </View>
                    </View>
                    <View style={styles.view}>
                        <View style={styles.view2}>
                            <Text style={styles.text2}
                            >Data Rekening
                            </Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('DataRekening')}>
                                <Text style={styles.text3}>
                                    Edit Data Rekening
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.line} />
                        <View style={{ paddingHorizontal: 12 }}>
                            <Text
                                style={styles.text}>
                                Bank
                            </Text>
                            <Text
                                style={styles.text2}>
                                BCA
                            </Text>
                            <Text style={styles.text}>
                                Nama Pemilik Rekening
                            </Text>
                            <Text
                                style={styles.text2}>
                                Setiawan
                            </Text>
                            <Text style={styles.text}>
                                No. Rekening
                            </Text>
                            <Text
                                style={styles.text2}>
                                123456789
                            </Text>
                        </View>
                    </View>
                    <View style={styles.view}>
                        <View style={styles.view2}>
                            <Text style={styles.text2}
                            >Dokumen
                            </Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('DataDokumen')}>
                                <Text style={styles.text3}>
                                    Edit Dokumen
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.line} />
                        <View style={{ paddingHorizontal: 12 }}>
                            <Text
                                style={styles.text}>
                                No KTP
                            </Text>
                            <Text
                                style={styles.text2}>
                                -
                            </Text>
                            <Text style={styles.text}>
                                Foto KTP
                            </Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../../asset/image/Profile/arimakana.jpg')}
                                    style={styles.image2}
                                />
                            </View>
                            <Text style={styles.text}>
                                No NPWP
                            </Text>
                            <Text
                                style={styles.text2}>
                                -
                            </Text>
                            <Text style={styles.text}>
                                Foto NPWP
                            </Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={require('../../../asset/image/Profile/arimakana.jpg')}
                                    style={styles.image2}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{ marginBottom: '60%' }}></View>
                </ScrollView>
            </View >

        </View>
    );
};

const styles = StyleSheet.create({
    name: {
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 5,
        alignSelf: 'center',
    },
    image: {
        width: 64,
        height: 64,
        resizeMode: 'center'
    },
    container: {
        // width: ,
        // height: ,
        borderColor: '#fff',
        borderWidth: 1,
        borderRadius: 75,
        overflow: 'hidden',
        alignSelf: 'center',
    },

    text: {
        marginTop: 10,
        fontSize: 12,
        color: '#002558',
    },
    text2: {
        color: '#002258',
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 15,
    },
    view: {
        backgroundColor: '#fff',
        borderRadius: 10,
        marginTop: 12,
    },
    view2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingHorizontal: 12,
    },
    text: {
        marginTop: 10,
        fontSize: 12,
        color: '#002558',
    },
    text2: {
        color: '#002258',
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 15,
    },
    text3: {
        fontSize: 15,
        color: '#287AE5',
        fontStyle: 'italic',
    },
    line: {
        borderColor: '#F7F7F7',
        borderRadius: 5,
        borderWidth: 1,
        width: '100%',
        marginVertical: 5
    },
    image2: {
        resizeMode: 'contain',
        width: 214,
        height: 128,
        borderColor: '#fff',
        borderWidth: 1,
        borderRadius: 10,
        marginVertical: 10
    },

})
export default ProfileComponent;
