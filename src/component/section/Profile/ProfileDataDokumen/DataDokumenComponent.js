import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  Alert,
  Image,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { TextRegular, TextBold, TextMedium, Header, InputText } from '../global';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { ScrollView } from 'react-native-gesture-handler';

const DataDokumenComponent = ({ navigation }) => {
  const SEARCH_WIDTH = Dimensions.get('window').width
  const SEARCH_HEIGHT = Dimensions.get('window').height
  const [show, setShow] = useState(null);

  const [imageData, setImageData] = useState(null);
  const pickImage = () => {
    launchImageLibrary({mediaType: 'photo', includeBase64: true}, response => {
      if (!response.didCancel && !response.errorCode) {
        const base64Data = `data:${response.assets[0].type};base64,${response.assets[0].base64}`;
        console.log('Image: ', response);
        setImageData(response.assets[0].uri);
      }
    });
  };


  return (
    <View style={{ flex: 1, backgroundColor: '#F7F7F7' }}>
      <View style={{ width: SEARCH_WIDTH, height: SEARCH_HEIGHT }}>
      <ScrollView>
        <View style={{ backgroundColor: 'white', margin: 16, }}>
          <View>
            <Text style={styles.text}>Data Dokumen</Text>
            <View style={styles.line} />
            <View style={{ padding: 12 }}>
              <Text style={styles.text2}>Nomor KTP</Text>
              <TextInput placeholder="123456789012345"
                style={styles.textinput} />
              <Text style={styles.text2}>Foto KTP</Text>
              <View style={styles.view3}>
                <View style={styles.view4}></View>
                <Text style={styles.text4}>UNGGAH KTP</Text>
              </View>
              <Text style={styles.text2}>Nomor NPWP</Text>
              <TextInput placeholder="123456789012345"
                style={styles.textinput} />
              <Text style={styles.text2}>Foto NPWP</Text>
              <View style={styles.view3}>
                <View style={styles.view4}></View>
                <Text style={styles.text4}>UNGGAH NPWP</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{marginBottom:'40%'}}></View>
        </ScrollView>
      </View>
      <TouchableOpacity
        style={styles.touchableopacity}>
        <Text style={styles.text3}>SIMPAN</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  textinput: {
    margin: 10,
    borderColor: '#E5E8EE',
    borderWidth: 1,
    width: '100%',
    alignSelf: 'center',
    color: '#002558',
    paddingHorizontal: 12,
  },
  view2: {
    margin: 10,
    borderColor: '#E5E8EE',
    borderWidth: 1,
    width: '100%',
    alignSelf: 'center',
    color: '#002558',
    paddingHorizontal: 12,
    flexDirection: 'row',
  },
  view3 : {
    margin: 10,
    width:'100%',
    borderColor:'#E5E8EE',
    borderWidth: 1,
    alignSelf: 'center',
    paddingHorizontal: 12,
    paddingVertical:8,
  },
  view4 : {height:128, resizeMode:'center', marginVertical:5},
  text: {
    fontSize: 15,
    color: '#002558',
    fontWeight: '700',
    padding: 12,
  },
  text2: { color: '#002558', },
  line: {
    backgroundColor: '#F7F7F7',
    width: '100%',
    height: 3,
    marginVertical: 5
  },
  text3: {
    backgroundColor: '#287AE5',
    paddingVertical: 9,
    borderRadius: 10,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white'
  },
  text4 : {
    width:'100%',
    textAlign:'center',
    backgroundColor:'#287AE5',
    color:'white',
    paddingVertical:6,
    borderRadius:10,
  },
  touchableopacity: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    paddingBottom: 20,
    paddingHorizontal: 20,
    width: '100%',
  },
})
//   const [imageData, setImageData] = useState(null);
//   const pickImage = () => {
//     launchImageLibrary({mediaType: 'photo', includeBase64: true}, response => {
//       if (!response.didCancel && !response.errorCode) {
//         const base64Data = `data:${response.assets[0].type};base64,${response.assets[0].base64}`;
//         console.log('Image: ', response);
//         setImageData(response.assets[0].uri);
//       }
//     });
//   };
//   return (
//     <View style={{flex: 1, backgroundColor: '#F7F7F7'}}>
//       <View
//         style={{
//           backgroundColor: '#fff',
//           width: 364,
//           height: 40,
//           marginTop: 16,
//           borderBottomColor: '#F7F7F7',
//           borderBottomWidth: 1,
//           alignSelf: 'center',
//           justifyContent: 'center',
//         }}>
//         <Text
//           style={{
//             fontSize: 15,
//             color: '#002558',
//             fontWeight: '700',
//             marginLeft: 10,
//           }}>
//           Data Dokumen
//         </Text>
//       </View>
//       <View
//         style={{
//           backgroundColor: '#fff',
//           width: 364,
//           alignSelf: 'center',
//         }}>
//         <Text
//           style={{
//             fontSize: 15,
//             color: '#002558',
//             marginTop: 5,
//             marginLeft: 10,
//           }}>
//           Nomor KTP
//         </Text>
//         <TextInput
//           placeholder="837166641555388" //pada tampilan ini, kita ingin user memasukkan email
//           style={{
//             marginTop: 10,
//             marginBottom: 10,
//             width: 346,
//             height: 35,
//             backgroundColor: '#fff',
//             borderColor: '#E5E8EE',
//             borderWidth: 1,
//             paddingHorizontal: 10,
//             alignSelf: 'center',
//           }}
//         />
//         <Text
//           style={{
//             fontSize: 15,
//             color: '#002558',
//             marginTop: 5,
//             marginLeft: 10,
//           }}>
//           Foto KTP
//         </Text>
//         <View
//           style={{
//             // backgroundColor: 'red',
//             borderColor: '#E5E8EE',
//             borderWidth: 1,
//             width: 346,
//             height: 150,
//             alignSelf: 'center',
//             marginTop: 5,
//           }}>
//           <Image
//             source={require('../../../../asset/image/Profile/backgroundprofile.png')}
//             style={{
//               width: '60%',
//               height: '60%',
//               alignSelf: 'center',
//               marginTop: 10,
//             }}
//           />
//           <TouchableOpacity
//             style={{marginTop: 10, width: 300, alignSelf: 'center'}}>
//           </TouchableOpacity>
//         </View>
//         <Text
//           style={{
//             fontSize: 15,
//             color: '#002558',
//             marginTop: 5,
//             marginLeft: 10,
//           }}>
//           Nomor npwp
//         </Text>
//         <TextInput
//           placeholder="1430493252js73hh"
//           style={{
//             marginTop: 10,
//             marginBottom: 10,
//             width: 346,
//             height: 35,
//             backgroundColor: '#fff',
//             borderColor: '#E5E8EE',
//             borderWidth: 1,
//             paddingHorizontal: 10,
//             alignSelf: 'center',
//           }}
//           keyboardType="numeric"
//           secureTextEntry={true}
//         />
//         <Text
//           style={{
//             fontSize: 15,
//             color: '#002558',
//             marginTop: 5,
//             marginLeft: 10,
//           }}>
//           Foto NPWP
//         </Text>
//         <View
//           style={{
//             // backgroundColor: 'red',
//             borderColor: '#E5E8EE',
//             borderWidth: 1,
//             width: 346,
//             height: 150,
//             alignSelf: 'center',
//             marginTop: 5,
//             marginBottom: 10,
//           }}>
//           <Image
//             source={require('../../../../asset/image/Profile/backgroundprofile.png')}
//             style={{
//               width: '60%',
//               height: '60%',
//               alignSelf: 'center',
//               marginTop: 10,
//             }}
//           />
//           <TouchableOpacity
//             style={{marginTop: 10, width: 300, alignSelf: 'center'}}>
//           </TouchableOpacity>
//         </View>
//       </View>
//       <View
//         style={{
//           flex: 1,
//           justifyContent: 'flex-end',
//           alignItems: 'center',
//           position: 'absolute',
//           bottom: 0,
//           left: 0,
//           right: 0,
//           paddingBottom: 20,
//           paddingHorizontal: 20,
//         }}>
//         <TouchableOpacity style={{width: 346}}>
//         </TouchableOpacity>
//       </View>
//     </View>
//   );
// };
export default DataDokumenComponent;
