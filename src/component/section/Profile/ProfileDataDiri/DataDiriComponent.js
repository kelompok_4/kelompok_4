import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Button,
  Alert,
  Modal,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { TextRegular, TextBold, TextMedium, Header, InputText } from '../global';
import AntDesign from 'react-native-vector-icons/AntDesign';

const DataDiriComponent = ({ navigation }) => {
  const SEARCH_WIDTH = Dimensions.get('window').width
  const SEARCH_HEIGHT = Dimensions.get('window').height
  const [show, setShow] = useState(null);

  return (
    <View style={{ flex: 1, backgroundColor: '#F7F7F7' }}>
      <View style={{ width: SEARCH_WIDTH, height: SEARCH_HEIGHT }}>
        <View style={{ backgroundColor: 'white', margin: 16, }}>
          <View>
            <Text style={styles.text}>Data Diri</Text>
            <View style={styles.line} />
            <View style={{ padding: 12 }}>
              <Text style={styles.text2}>Nama Kepala Cabang</Text>
              <TextInput placeholder="Sulthan Alfi"
                style={styles.textinput} />
              <Text style={styles.text2}>No. Telepon</Text>
              <TextInput placeholder="081245651200"
                style={styles.textinput} />
              <Text style={styles.text2}>Tanggal Lahir</Text>
              <View style={styles.view2}>
                <View style={{ width: '90%' }}>
                  <TextInput placeholder="07-04-1980" />
                </View>
                <View style={{ flexDirection: 'row-reverse' }}>
                  <AntDesign name="calendar" size={25} color="black" style={{ alignSelf: 'center' }} />
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
        <TouchableOpacity
          style={styles.touchableopacity}>
          <Text style={styles.text3}>SIMPAN</Text>
        </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  textinput: {
    margin: 10,
    borderColor: '#E5E8EE',
    borderWidth: 1,
    width: '100%',
    alignSelf: 'center',
    color: '#002558',
    paddingHorizontal: 12,
  },
  view2: {
    margin: 10,
    borderColor: '#E5E8EE',
    borderWidth: 1,
    width: '100%',
    alignSelf: 'center',
    color: '#002558',
    paddingHorizontal: 12,
    flexDirection: 'row',
  },
  text: {
    fontSize: 15,
    color: '#002558',
    fontWeight: '700',
    padding: 12,
  },
  text2: { color: '#002558', },
  line: {
    backgroundColor: '#F7F7F7',
    width: '100%',
    height: 3,
    marginVertical: 5
  },
  text3: {
    backgroundColor: '#287AE5',
    paddingVertical: 9,
    borderRadius: 10,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white'
  },
  touchableopacity: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    paddingBottom: 20,
    paddingHorizontal: 20,
    width: '100%',
  },
})
export default DataDiriComponent;
