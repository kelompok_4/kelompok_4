import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/AntDesign';
import Iconiza from 'react-native-vector-icons/FontAwesome5';

const BeliMobilComponent = ({navigation, route}) => {
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F7F7F7',
      }}>
      <View
        style={{
          flexDirection: 'row',
          padding: 19,
          elevation: 1,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          style={{paddingRight: 14}}
          onPress={() => navigation.navigate('BotNav')}>
          <Icon name="arrowleft" size={25} color={'black'} />
        </TouchableOpacity>
        <Text
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            fontSize: 18,
            color: '#002558',
            fontWeight: 'bold',
            marginLeft: 90,
          }}>
          Beli Mobil
        </Text>
        <TouchableOpacity>
          <Iconiza
            name="share-alt"
            size={25}
            color={'#002558'}
            style={{
              marginLeft: 120,
            }}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: '100%',
          backgroundColor: '#FFFFFF',
          height: 45,
          marginTop: 5,
        }}>
        <Text
          style={{
            color: '#002558',
            marginTop: 6,
            marginLeft: 15,
            fontSize: 15,
          }}>
          TR-092018-246
        </Text>
        <Text
          style={{
            color: '#002558',
            marginLeft: 210,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            fontSize: 15,
            marginTop: -22,
          }}>
          Sen, 17 Sep 2018 - 10:30
        </Text>
        <View
          style={{
            borderStyle: 'solid',
            borderWidth: 0.3,
            marginTop: 20,
            borderColor: '#002558',
          }}></View>
      </View>
      <View
        style={{
          width: '100%',
          height: 120,
          backgroundColor: '#FFFFFF',
          marginTop: 0.5,
        }}>
        <Text
          style={{
            marginLeft: 20,
            fontSize: 15,
            marginTop: 20,
            color: '#002558',
          }}>
          Nama Customer
        </Text>
        <Text
          style={{
            marginLeft: 200,
            fontSize: 15,
            marginTop: -20,
            fontWeight: 'bold',
            color: '#002558',
          }}>
          NANDO DWIKI SATRIA
        </Text>
        <Text
          style={{
            marginLeft: 20,
            fontSize: 15,
            marginTop: 20,
            color: '#002558',
          }}>
          No.Hp
        </Text>
        <Text
          style={{
            marginLeft: 252,
            fontSize: 15,
            marginTop: -20,
            color: '#002558',
          }}>
          082245884655
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          backgroundColor: '#FFFFFF',
          height: 45,
          marginTop: 10,
        }}>
        <Text
          style={{
            color: '#002558',
            marginLeft: 22,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            fontSize: 15,
            marginTop: 10,
            fontWeight: 'bold',
          }}>
          Detail Mobil
        </Text>
        <View
          style={{
            borderStyle: 'solid',
            borderWidth: 1,
            marginTop: 17,
            borderColor: '#002558',
          }}></View>
      </View>
      <View
        style={{
          width: '100%',
          height: 120,
          backgroundColor: '#FFFFFF',
          marginTop: 2.5,
        }}>
        <Text
          style={{
            marginLeft: 20,
            fontSize: 15,
            marginTop: 20,
            color: '#002558',
          }}>
          New Car
        </Text>
        <Text
          style={{
            marginLeft: 21,
            fontSize: 15,
            marginTop: 5,
            color: '#002558',
            fontWeight: 'bold',
          }}>
          Rush S AT TRD
        </Text>
        <Text
          style={{
            marginLeft: 255,
            fontSize: 15,
            marginTop: -25,
            color: '#002558',
            fontWeight: 'bold',
          }}>
          Rp 276.600.000
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          backgroundColor: '#FFFFFF',
          height: 45,
          marginTop: 10,
        }}>
        <Text
          style={{
            color: '#002558',
            marginLeft: 22,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
            fontSize: 15,
            marginTop: 10,
            fontWeight: 'bold',
          }}>
          Diskon
        </Text>
        <View
          style={{
            borderStyle: 'solid',
            borderWidth: 1,
            marginTop: 17,
            borderColor: '#002558',
          }}></View>
      </View>
      <View
        style={{
          width: '100%',
          height: 90,
          backgroundColor: '#FFFFFF',
          marginTop: 2.5,
        }}>
        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          style={{
            width: '90%',
            marginTop: 30,
            backgroundColor: '#287AE5',
            borderRadius: 5,
            paddingVertical: 8,
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 20,
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 18,
              marginRight: 140,
            }}>
            REQUEST DISCOUNT
          </Text>
          <Icon
            name="arrowright"
            size={23}
            color={'white'}
            style={{
              marginLeft: 300,
              marginTop: -23,
            }}
          />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={{
          width: '40%',
          marginTop: 95,
          backgroundColor: '#CCD3DD',
          borderRadius: 5,
          paddingVertical: 8,
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 28,
        }}>
        <Text
          style={{
            color: '#002558',
            fontSize: 18,
          }}>
          NO DEAL
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: '40%',
          backgroundColor: '#287AE5',
          borderRadius: 5,
          paddingVertical: 8,
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 210,
          marginTop: -41,
        }}>
        <Text
          style={{
            color: '#fff',
            fontSize: 18,
          }}>
          DEAL
        </Text>
      </TouchableOpacity>
      <Modal
        isVisible={isModalVisible}
        onBackdropPress={toggleModal}
        style={{
          justifyContent: 'flex-end',
          margin: 0,
        }}>
        <View
          style={{
            width: '100%',
            height: 450,
            backgroundColor: 'white',
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              color: '#002558',
              fontSize: 22,
              paddingLeft: 16,
              paddingTop: 25,
            }}>
            Request Discount
          </Text>
          <TouchableOpacity onPress={() => setModalVisible(false)}>
            <Icon
              name="close"
              size={25}
              color={'#002558'}
              style={{
                paddingLeft: 350,
                marginTop: -30,
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              width: '100%',
              borderStyle: 'solid',
              borderWidth: 0.19,
              marginTop: 10,
              borderColor: '#002558',
            }}></View>
          <Text
            style={{
              color: '#002558',
              fontWeight: '600',
              paddingTop: 20,
              paddingHorizontal: 23,
              fontSize: 17,
            }}>
            Masukan Diskon
          </Text>
          <Text
            style={{
              color: 'gray',
              fontWeight: '600',
              marginTop: -18,
              paddingLeft: 240,
              fontSize: 13,
            }}>
            OTR :
          </Text>
          <Text
            style={{
              color: '#002558',
              fontWeight: 'bold',
              marginTop: -18,
              paddingLeft: 280,
              fontSize: 13,
            }}>
            Rp. 276.600.000
          </Text>
          <View
            style={{
              width: '92%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderWidth: 2,
              borderColor: '#D6D6D6',
              backgroundColor: 'white',
              borderRadius: 5,
              marginLeft: 18,
              paddingLeft: 20,
              paddingVertical: 7,
              alignContent: 'center',
              marginTop: 10,
              paddingRight: 200,
            }}>
            <Text
              style={{
                color: '#002558',
                fontSize: 20,
                fontWeight: '500',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              Rp
            </Text>
            <Text
              style={{
                color: '#287AE5',
                fontSize: 20,
                fontWeight: 'bold',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              15.000.000
            </Text>
          </View>
          <Text
            style={{
              color: 'gray',
              fontSize: 13,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              marginTop: 10,
              marginLeft: 25,
            }}>
            * Request Pengajuan Discount
          </Text>
          <Text
            style={{
              color: '#002558',
              fontSize: 15,
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              marginTop: 10,
              marginLeft: 30,
            }}>
            Notes
          </Text>
          <Text
            style={{
              color: 'gray',
              fontSize: 15,
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
              marginTop: -20,
              marginLeft: 340,
            }}>
            0/100
          </Text>
          <View
            style={{
              width: '92%',
              borderWidth: 1,
              borderColor: '#D6D6D6',
              backgroundColor: 'white',
              borderRadius: 5,
              paddingVertical: 60,
              marginLeft: 20,
              marginTop: 10,
            }}>
            <TextInput
              placeholder="Masukkan Catatan"
              maxLength={100}
              style={{
                marginTop: -65,
                marginLeft: 10,
                fontSize: 18,
              }}></TextInput>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('RequestDalamProses')}
            style={{
              width: '90%',
              marginTop: 30,
              backgroundColor: '#287AE5',
              borderRadius: 5,
              paddingVertical: 8,
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: 20,
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 18,
              }}>
              SEND REQUEST
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  );
};

export default BeliMobilComponent;
