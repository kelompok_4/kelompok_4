import React, { useState } from 'react';
import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Dimensions,
  StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ModalTracking from '../../modal/Tracking/ModalTracking';

const HomeComponent = ({ navigation, route }) => {

  const SEARCH_WIDTH = Dimensions.get('window').width - 93
  const [show, setShow] = useState(null);
  const [index, setIndex] = useState(1);
  const [searching, setSearching] = useState('')

  const [modalTracking, setModalTracking] = useState('')

  const [available, setAvailable] = useState('');
  const handleOpen = () => {
    setAvailable('');
  };
  const handleClose = () => {
    setAvailable('Close');
  };

  return (
    <View style={{ flex: 1, backgroundColor: '#F7F7F7' }}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 10 }}>
        <KeyboardAvoidingView
          // behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <ImageBackground
            source={require('../../../asset/image/biru.png')}
            style={{
              width: '100%',
              height: 316,
            }}>
            <View style={{
              marginHorizontal: 16,
              marginTop: 16
            }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 15,
                }}> BOH
              </Text>
              <Text
                style={{
                  color: 'white',
                  fontSize: 23,
                  fontWeight: '500',
                }}>Home
              </Text>
              <View
                style={{
                  backgroundColor: 'white',
                  padding: 12,
                  borderRadius: 7,
                  marginTop: 16,
                  // height:199,
                }}>
                <Text
                  style={{
                    fontSize: 19,
                    color: 'black',
                    justifyContent: 'center',
                  }}>
                  1 Juni - 12 Juni 2023
                </Text>
                <View
                  style={{
                    marginTop: 12,
                    borderRadius: 10,
                    borderWidth: 1,
                    borderColor: 'rgba(0,0,0,0.2)'
                  }} />
                <View style={{ flexDirection: 'row', marginTop: 12, justifyContent: 'space-between' }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('CekHarga')}
                    style={{
                      backgroundColor: '#F7F7F7',
                      borderRadius: 5,
                      width: '45%'
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View
                        style={{
                          width: '1%',
                          height: '35%',
                          backgroundColor: '#287AE5',
                          marginTop: 5,
                        }}></View>
                      <View style={{ padding: 10 }}>
                        <Text>
                          Cek Harga
                        </Text>
                        <Text
                          style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            color: '#002558',
                          }}>
                          32
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('TotalAppraiser')}
                    style={{
                      backgroundColor: '#F7F7F7',
                      borderRadius: 5,
                      width: '45%'
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View
                        style={{
                          width: '1%',
                          height: '35%',
                          backgroundColor: '#3AD647',
                          marginTop: 5,
                        }}></View>
                      <View style={{ padding: 10 }}>
                        <Text>
                          Total Appraisal
                        </Text>
                        <Text
                          style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            color: '#002558',
                          }}>
                          16
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 12, justifyContent: 'space-between' }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('PO')}
                    style={{
                      backgroundColor: '#F7F7F7',
                      borderRadius: 5,
                      width: '45%'
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View
                        style={{
                          width: '1%',
                          height: '35%',
                          backgroundColor: '#FFCD27',
                          marginTop: 5,
                        }}></View>
                      <View style={{ padding: 10 }}>
                        <Text>
                          PO
                        </Text>
                        <Text
                          style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            color: '#002558',
                          }}>
                          12
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('PoValid')}
                    style={{
                      backgroundColor: '#F7F7F7',
                      borderRadius: 5,
                      width: '45%'
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View
                        style={{
                          width: '1%',
                          height: '35%',
                          backgroundColor: '#FF6127',
                          marginTop: 5,
                        }}></View>
                      <View style={{ padding: 10 }}>
                        <Text>
                          PO Valid
                        </Text>
                        <Text
                          style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            color: '#002558',
                          }}>
                          4
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ImageBackground>
          <View style={{
            marginTop: 16,
            flexDirection: 'row',
            justifyContent: 'center',
            marginHorizontal:16,
            paddingHorizontal:16,
          }}>
            <View style={{
              backgroundColor: '#FFFFFF',
              height: 48,
              flexDirection: 'row',
              justifyContent:'space-between',
              borderRadius: 8,
              width:SEARCH_WIDTH, }}>
              <TextInput placeholder='Telusuri....'
                style={{
                  padding:14
                }}
                onChangeText={(text) => setSearching(text)}
                value={searching}
              ></TextInput>
              <TouchableOpacity onPress={() => ''}
                style={{ alignSelf: 'center' }}>
                <AntDesign name="search1" size={30} color="black" style={{
                  marginRight:10
                }} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => setModalTracking(true)}>
              <MaterialCommunityIcons name="tune" size={25} color="black"
                style={{
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                  padding: 12,
                  marginLeft: 12,
                }} />
            </TouchableOpacity>
          </View>
          <View style={{
            backgroundColor: '#FFFFFF',
            paddingHorizontal: 12,
            borderRadius: 8,
            marginTop: 12,
            flexDirection: 'row',
            alignSelf: 'center',
            marginHorizontal: 16,
          }}>
            <TouchableOpacity onPress={() => handleOpen()}
              style={styles.buttonTrade}>
              {available === '' ?
                <Text style={{ color: '#287AE5' }}>Trade In</Text>
                :
                available === 'Close' ?
                  <Text style={{ color: 'grey' }}>Trade In</Text>
                  : ""}
            </TouchableOpacity>
            <View style={{
              borderColor: 'rgba(0,0,0,0.1)',
              borderWidth: 1,
              height: '100%',

            }} />
            <TouchableOpacity onPress={() => handleClose()}
              style={styles.buttonTrade}>
              {available === '' ?
                <Text style={{ color: 'grey' }}>New Car</Text>
                :
                available === 'Close' ?
                  <Text style={{ color: '#287AE5' }}>New Car</Text>
                  : ""}
            </TouchableOpacity>
          </View>
          {available === '' ?
            <View
              style={{
                flex: 1,
                backgroundColor: '#F7F7F7',
                alignItem: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => setShow(show === 2 ? null : 2)}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderWidth: 1,
                  borderColor: 'blue',
                  backgroundColor: 'white',
                  paddingVertical:7,
                  marginHorizontal:16,
                  marginTop: 10,
                }}>
                <View style={{}}>
                  <Text
                    style={{
                      fontSize: 17,
                      fontWeight: '600',
                      color: 'black',
                      marginLeft: 15,
                    }}>
                    New Approval
                  </Text>
                </View>
                <View>
                  <Icon
                    name={show === 2 ? 'up' : 'down'}
                    size={17}
                    color={'black'}
                    style={{
                      marginRight: 15,
                      marginTop: 2,
                    }}
                  />
                </View>
              </TouchableOpacity>
              {show === 2 ? (
                <View
                  style={{
                    backgroundColor: '#fff',
                    elevation: 9,
                    marginHorizontal: 16,
                    padding:10,

                  }}>
                  <TouchableOpacity>
                    <Text
                      style={{
                        fontSize: 15,
                        color: 'black',
                        fontWeight: '300',
                      }}>
                      hahahh
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
            :
            available === 'Close' ?
              ""
              : ""}
          {
            available === '' ?
              <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={{ position: 'relative', paddingHorizontal: 16, paddingTop: 12 }}>
                <TouchableOpacity onPress={()=>navigation.navigate('ToolsTradeIn')}
                  style={{
                    backgroundColor: 'white',
                    height: 'auto',
                    width: '100%',
                    borderRadius: 8,
                    padding: 12,
                    borderColor: 'rgba(0,0,0,0.2)',
                    borderWidth: 1,
                  }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.idNewCar}>TR-092018-246</Text>
                    <Text style={styles.tanggal}>Sen, 17 Sep 2018 - 10:30</Text>
                  </View>
                  <Text style={styles.namaMobil}>Avanza G 2.0</Text>
                  <View style={styles.garisPembatas}></View>
                  <View style={{ marginTop: 10, flexDirection: 'row', justifyContent:'space-between' }}>
                    <View>
                      <Text style={styles.namaPekerja}>Handoko</Text>
                      <Text style={{
                        fontSize: 10,
                        color: 'rgba(0, 37, 88, 0.5)',
                      }}>Salesman</Text>
                    </View>
                    <View style={{ flexDirection: 'row-reverse' }}>
                      <Text style={{
                        backgroundColor: '#287AE5',
                        textAlign: 'right',
                        color: '#FFFFFF',
                        fontSize: 10,
                        borderRadius: 10,
                        padding: 5,
                        alignSelf: 'center'
                      }}
                      >New Approval</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </ScrollView >
              :
              available === 'Close' ?
                <ScrollView
                  contentInsetAdjustmentBehavior="automatic"
                  style={{ position: 'relative', paddingHorizontal: 16, paddingTop: 12 }} >
                  <TouchableOpacity onPress={()=>navigation.navigate('BeliMobil')}
                    style={{
                      backgroundColor: 'white',
                      height: 'auto',
                      width: '100%',
                      borderRadius: 8,
                      padding: 12,
                      borderColor: 'rgba(0,0,0,0.2)',
                      borderWidth: 1,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.idNewCar}>TR-092018-246</Text>
                      <Text style={styles.tanggal}>Sen, 17 Sep 2018 - 10:30</Text>
                    </View>
                    <Text style={styles.namaMobil}>Avanza G 2.0</Text>
                    <View style={styles.garisPembatas}></View>
                    <View style={{ marginTop: 10, flexDirection: 'row', justifyContent:'space-between'  }}>
                      <View>
                        <Text style={styles.namaPekerja}>Handoko</Text>
                        <Text style={styles.pekerjaan}>Salesman</Text>
                      </View>
                      <View style={{ flexDirection: 'row-reverse' }}>
                        <Text style={{
                          backgroundColor: '#287AE5',
                          textAlign: 'right',
                          color: '#FFFFFF',
                          fontSize: 10,
                          borderRadius: 10,
                          padding: 5,
                          alignSelf: 'center'
                        }}
                        >New Approval</Text>
                        {/* <Text style={styles.sudahRequest}
                        >Request Diskon Sudah Terupdate</Text> */}
                      </View>
                    </View>
                  </TouchableOpacity>
                </ScrollView >
                : ""
          }
          <ModalTracking
            show={modalTracking}
            onClose={() => setModalTracking(false)}
            title='Filter' >
            <View style={{ padding: 10 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, borderBottomWidth: 0.5, borderBottomColor: '#ccc' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>Request Diskon</Text>
                <FontAwesome name="circle-o" size={16} color="#002558" />
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>Request Subsidi</Text>
                <FontAwesome name="dot-circle-o" size={16} color="#002558" />
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>Request MRP</Text>
                <FontAwesome name="dot-circle-o" size={16} color="#002558" />
              </View>
              <View style={{ width: '100%', padding: 10 }}>
                <Text style={{
                  backgroundColor: '#287AE5',
                  fontWeight: 'bold',
                  fontSize: 16,
                  color: 'white',
                  textAlign: 'center',
                  padding: 5,
                  borderRadius: 8
                }}>SIMPAN</Text>
              </View>
            </View>
          </ModalTracking>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonTrade: {
    paddingVertical: 9,
    width: '50%',
    alignItems: 'center',
    borderColor: '#0000000F',
  },
  idNewCar: {
    fontSize: 12,
    textAlign: 'left',
    color: 'rgba(0, 37, 88, 0.5)',
  },
  tanggal: {
    fontSize: 12,
    color: '#002558',
    right: 0,
    position: 'absolute'
  },
  namaMobil: {
    fontSize: 14,
    marginTop: 8,
    color: '#002558',
    fontWeight: 'bold'
  },
  garisPembatas: {
    marginTop: 10,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 5
  },
  namaPekerja: {
    fontSize: 12,
    color: '#002558',
    fontWeight: 'bold',
  },
  pekerjaan: {
    fontSize: 10,
    color: 'rgba(0, 37, 88, 0.5)',
  },
  dalamRequest: {
    backgroundColor: '#002558',
    textAlign: 'right',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center'
  },
  sudahRequest: {
    backgroundColor: '#FF6127',
    textAlign: 'right',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center'
  }

})

export default HomeComponent;
