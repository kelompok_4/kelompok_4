import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Dimensions,
  StyleSheet
} from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ModalTracking from '../../../modal/Tracking/ModalTracking';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const TotalAppraisalComponent = ({ navigation, route }) => {
  const SEARCH_WIDTH = Dimensions.get('window').width
  const SEARCH_HEIGHT = Dimensions.get('window').height
  const [searching, setSearching] = useState('')

  const [modalTracking, setModalTracking] = useState('')
  const [status, setStatus] = useState('')

  const [available, setAvailable] = useState('');
  const handleOpen = () => {
    setAvailable('');
  };
  const handleClose = () => {
    setAvailable('Close');
  };

  return (
    <View style={{ flex: 1, backgroundColor: '#F7F7F7' }}>
      <View style={{ height: SEARCH_HEIGHT, width: SEARCH_WIDTH }}>
        <View
          style={{
            width: '100%',
            height: 160,
            elevation: 3,
            backgroundColor: '#287AE5',
            padding: 20,
          }}>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => navigation.goBack('Home')}>
              <AntDesign
                name="arrowleft"
                size={30}
                color={'white'}
              />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 20,
                color: 'white',
                fontWeight: 'bold',
                textAlignVertical: 'center',
                marginLeft: 10,
              }}>
              Cek Harga
            </Text>
          </View>
          <View style={{
            flexDirection: 'row',
            height: 48,
            marginTop: 31,
            width: '100%',
            justifyContent: 'space-between',
          }}>
            <View style={{
              backgroundColor: '#FFFFFF',
              height: 48,
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderRadius: 8,
              paddingHorizontal: 5,
              width: '80%',
            }}>
              <TextInput placeholder='Telusuri....'
                onChangeText={(text) => setSearching(text)}
                value={searching}></TextInput>
              <TouchableOpacity onPress={() => ''}
                style={{ alignSelf: 'center' }}>
                <AntDesign name="search1" size={30} color="black" />
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => setModalTracking(true)}>
              <MaterialCommunityIcons name="tune" size={25} color="black"
                style={{
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                  padding: 12,
                  marginLeft: 12,
                }} />
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={{ paddingHorizontal: 16, paddingTop: 12 }} >
          <TouchableOpacity onPress={() => navigation.navigate('ToolsTradeIn')}
            style={{
              backgroundColor: 'white',
              height: 'auto',
              width: '100%',
              borderRadius: 8,
              padding: 12,
              borderColor: 'rgba(0,0,0,0.2)',
              borderWidth: 1,
            }}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.idNewCar}>TR-092018-246</Text>
              <Text style={styles.tanggal}>Sen, 17 Sep 2018 - 10:30</Text>
            </View>
            <Text style={styles.namaMobil}>Avanza G 2.0</Text>
            <View style={styles.garisPembatas}></View>
            <View style={styles.view2}>
              <View>
                <Text style={styles.namaPekerja}>Handoko</Text>
                <Text style={styles.text2}>Salesman</Text>
              </View>
              <View style={{ flexDirection: 'row-reverse' }}>
                {status === 'Sedang Dalam Request' ? (
                  <Text style={styles.text3}>Sedang Dalam Request</Text>
                ) : status === 'Request MRP Sudah Terupdate' ? (
                  <Text style={styles.text4}>Request MRP Sudah Terupdate</Text>
                ) : status === 'Request Subsudi Sudah Terupdate' ? (
                  <Text style={styles.text5}>Request Subsudi Sudah Terupdate</Text>
                ) : status === 'Request Diskon Sudah Terupdate' ? (
                  <Text style={styles.text6}>Request Diskon Sudah Terupdate</Text>
                ) : null}
              </View>
            </View>
          </TouchableOpacity>
          <ModalTracking
            show={modalTracking}
            onClose={() => setModalTracking(false)}
            title='Filter' >
            <View style={{ padding: 10 }}>
              <View style={styles.view3}>
                <Text style={styles.text8}>Request Diskon</Text>
                <FontAwesome name="circle-o" size={16} color="#002558" />
              </View>
              <View style={styles.view4}>
                <Text style={styles.text8}>Request Subsidi</Text>
                <FontAwesome name="dot-circle-o" size={16} color="#002558" />
              </View>
              <View style={styles.view4}>
                <Text style={styles.text8}>Request MRP</Text>
                <FontAwesome name="dot-circle-o" size={16} color="#002558" />
              </View>
              <View style={{ width: '100%', padding: 10 }}>
                <Text style={styles.text7}>SIMPAN</Text>
              </View>
            </View>
          </ModalTracking>
        </ScrollView >
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonTrade: {
    paddingVertical: 9,
    width: '50%',
    alignItems: 'center',
    borderColor: '#0000000F',
  },
  idNewCar: {
    fontSize: 12,
    textAlign: 'left',
    color: 'rgba(0, 37, 88, 0.5)',
  },
  tanggal: {
    fontSize: 12,
    color: '#002558',
    right: 0,
    position: 'absolute'
  },
  namaMobil: {
    fontSize: 14,
    marginTop: 8,
    color: '#002558',
    fontWeight: 'bold'
  },
  garisPembatas: {
    marginTop: 10,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: 'rgba(0,0,0,0.3)',
  },
  namaPekerja: {
    fontSize: 12,
    color: '#002558',
    fontWeight: 'bold',
  },
  pekerjaan: {
    fontSize: 10,
    color: 'rgba(0, 37, 88, 0.5)',
  },
  dalamRequest: {
    backgroundColor: '#002558',
    textAlign: 'right',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center'
  },
  sudahRequest: {
    backgroundColor: '#FF6127',
    textAlign: 'right',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center'
  },
  view2: { marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' },
  text2: {
    fontSize: 10,
    color: 'rgba(0, 37, 88, 0.5)',
  },
  text3: {
    backgroundColor: '#002558',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center',
    textAlign: 'right',
  },
  text4: {
    backgroundColor: '#FFCF32',
    textAlign: 'right',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center'
  },
  text5: {
    backgroundColor: '#A4D166',
    textAlign: 'right',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center'
  },
  text6: {
    backgroundColor: '#287AE5',
    textAlign: 'right',
    color: '#FFFFFF',
    fontSize: 10,
    borderRadius: 10,
    padding: 5,
    alignSelf: 'center'
  },
  text7: {
    backgroundColor: '#287AE5',
    fontWeight: 'bold',
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
    padding: 5,
    borderRadius: 8
  },
  text8: { fontWeight: 'bold', fontSize: 16, color: '#002558' },
  view3: { flexDirection: 'row', justifyContent: 'space-between', padding: 10, borderBottomWidth: 0.5, borderBottomColor: '#ccc' },
  view4: { flexDirection: 'row', justifyContent: 'space-between', padding: 10 },
})

export default TotalAppraisalComponent;