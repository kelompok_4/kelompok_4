import React, { useState } from 'react';
import { Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    Image,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
const MobileComponent = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width
    const SEARCH_HEIGHT = Dimensions.get('window').height

    return (
        <View style={styles.box}>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '50%' }}>
                    <Image
                        style={{ height: 100, width: '100%', resizeMode: 'contain' }}
                        source={require('../../../../asset/image/Summary/DetailAppraisal3_2.png')}
                    />
                </View>
                <View style={{ width: '35%' }}>
                    <Text style={{
                        color: '#1B385D',
                        fontWeight: 'bold',
                        fontSize: 16,
                        paddingBottom: 9,
                    }}>Avanza tipe G/AT 2015</Text>
                    <Text style={{
                        color: '#005596',
                        fontSize: 12,
                        paddingBottom: 4,
                    }}>Harga Final :</Text>
                    <Text style={{
                        color: '#005596',
                        fontSize: 16,
                        fontWeight: 'bold'
                    }}>76.500.000</Text>
                </View>
            </View>
            <View style={{
                marginVertical: 5,
                borderColor: '#F2F6FA',
                height: 1,
                borderWidth: 1,
            }} />
            <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '50%' }}>
                    <View style={styles.viewIconTop}>
                        <Image
                            style={styles.iconTop}
                            source={require('../../../../asset/image/Summary/DetailAppraisal3_3.png')}
                        />
                        <Text style={{ fontSize: 10 }}>Bebas Banjir</Text>
                    </View>
                </View>
                <View style={{ width: '50%' }}>
                    <View style={styles.viewIconTop}>
                        <Image
                            style={styles.iconTop}
                            source={require('../../../../asset/image/Summary/DetailAppraisal3_4.png')}
                        />
                        <Text style={{ fontSize: 10 }}>Bebas Kecelakaan</Text>
                    </View>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    viewIconTop: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconTop: {
        height: 50,
        resizeMode: 'contain'
    },
    kondisi: {
        backgroundColor: '#EBFDF5',
        paddingVertical: 12,
        marginHorizontal: 12,
        color: '#1B385D',
        fontWeight: 'bold',
        marginTop: 12,
        textAlign: 'center'
    },
    title: {
        color: '#1B385D',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
    },
    box: {
        borderRadius: 8,
        backgroundColor: 'white',
        marginTop: 15,
        paddingTop: 12,
        marginBottom: 15,
        paddingBottom: 12,
    },
    line: {
        marginVertical: 10,
        borderColor: '#F2F6FA',
        height: 1,
        borderWidth: 1,
    },
    view: {
        marginTop: 13,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 12
    },
    text: {
        color: '#005596',
        fontSize: 12
    },
    text2: {
        color: '#1B385D',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    text3: {
        color: '#E5394B',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    icon: {
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    iconText: {
        color: '#1B385D',
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10,
    },
    viewIcon: {
        flexDirection: 'row',
        paddingHorizontal: 12,
        marginTop: 26,
    },
})


export default MobileComponent;
