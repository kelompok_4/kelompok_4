import React, { useState } from 'react';
import { Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    Image,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
const HasilPerhitunganFinal = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width
    const SEARCH_HEIGHT = Dimensions.get('window').height

    return (
        <View style={styles.box}>
            <Text style={styles.title}
            >Hasil Penghitungan Final</Text>
            <View style={styles.line} />
            <View style={{ flexDirection: 'row', paddingHorizontal: 12 }}>
                <Image
                    style={styles.icon}
                    source={require('../../../../asset/image/Summary/DetailAppraisal3_5.png')}
                />
                <Text style={styles.iconText}>Overview</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Nomor Polisi</Text>
                <Text style={styles.text2}>B 1234 XYZ</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Nomor Mesin</Text>
                <Text style={styles.text2}>161613516156451325</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Nomor Rangka</Text>
                <Text style={styles.text2}>72149GHG68</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Warna Eksterior</Text>
                <Text style={styles.text2}>Putih</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Jarak Tempuh</Text>
                <Text style={styles.text2}>40.000</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Tahun STNK</Text>
                <Text style={styles.text2}>2017</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Kilometer</Text>
                <View>
                    <Text style={styles.text2}>Diatas 100.000 Km</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>R.Mesin</Text>
                <View>
                    <Text style={styles.text2}>Oli Rembes</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Banjir</Text>
                <View>
                    <Text style={styles.text2}>Ya</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.viewIcon}>
                <Image
                    style={styles.icon}
                    source={require('../../../../asset/image/Summary/DetailAppraisal3_6.png')}
                />
                <Text style={styles.iconText}>Kendaraan</Text>
            </View>
            <Text style={styles.kondisi}>Kendaraan dalam kondisi baik</Text>
            <View style={styles.viewIcon}>
                <Image
                    style={styles.icon}
                    source={require('../../../../asset/image/Summary/DetailAppraisal3_7.png')}
                />
                <Text style={styles.iconText}>Frame</Text>
            </View>
            <Text style={styles.kondisi}>Frame dalam kondisi baik</Text>
            <View style={styles.viewIcon}>
                <Image
                    style={styles.icon}
                    source={require('../../../../asset/image/Summary/DetailAppraisal3_8.png')}
                />
                <Text style={styles.iconText}>Engine</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Engine Defect</Text>
                <View>
                    <Text style={styles.text2}>Perlu perbaikan Engine Oil Leakage</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Engine Defect</Text>
                <View>
                    <Text style={styles.text2}>Perlu perbaikan White Smoke</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.viewIcon}>
                <Image
                    style={styles.icon}
                    source={require('../../../../asset/image/Summary/DetailAppraisal3_8.png')}
                />
                <Text style={styles.iconText}>Interior</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Small</Text>
                <View>
                    <Text style={styles.text2}>Interior Kotor</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.viewIcon}>
                <Image
                    style={styles.icon}
                    source={require('../../../../asset/image/Summary/DetailAppraisal3_10.png')}
                />
                <Text style={styles.iconText}>Eksterior</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Front</Text>
                <View>
                    <Text style={styles.text2}>Perlu perbaikan Front Spoiler</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Front</Text>
                <View>
                    <Text style={styles.text2}>Perlu perbaikan Front Bumper</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>Front</Text>
                <View>
                    <Text style={styles.text2}>Perlu perbaikan Rear Bumper</Text>
                    <Text style={styles.text3}>- Rp.800.000</Text>
                </View>
            </View>
            <View style={styles.line} />
            <View style={styles.view}>
                <Text style={styles.text2}>Total pengurangan</Text>
                <Text style={styles.text3}>- Rp.6.400.000</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text2}>Harga Penawaran</Text>
                <Text style={{ font: 12, color: 'blue' }}>87.900.000</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    viewIconTop: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconTop: {
        height: 50,
        resizeMode: 'contain'
    },
    kondisi: {
        backgroundColor: '#EBFDF5',
        paddingVertical: 12,
        marginHorizontal: 12,
        color: '#1B385D',
        fontWeight: 'bold',
        marginTop: 12,
        textAlign: 'center'
    },
    title: {
        color: '#1B385D',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
    },
    box: {
        borderRadius: 8,
        backgroundColor: 'white',
        marginTop: 15,
        paddingTop: 12,
        marginBottom: 15,
        paddingBottom: 12,
    },
    line: {
        marginVertical: 10,
        borderColor: '#F2F6FA',
        height: 1,
        borderWidth: 1,
    },
    view: {
        marginTop: 13,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 12
    },
    text: {
        color: '#005596',
        fontSize: 12
    },
    text2: {
        color: '#1B385D',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    text3: {
        color: '#E5394B',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    icon: {
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    iconText: {
        color: '#1B385D',
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10,
    },
    viewIcon: {
        flexDirection: 'row',
        paddingHorizontal: 12,
        marginTop: 26,
    },
})


export default HasilPerhitunganFinal;
