import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/AntDesign';
import Iconi from 'react-native-vector-icons/MaterialCommunityIcons';

const TotalAppraiserComponent = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F7F7F7'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: '100%',
            height: 160,
            flexDirection: 'row',
            elevation: 3,
            backgroundColor: '#287AE5',
          }}>
          <TouchableOpacity onPress={() => navigation.goBack('Home')}>
            <Icon
              name="arrowleft"
              size={30}
              color={'white'}
              style={{
                marginLeft: 20,
                marginTop: 30,
              }}
            />
          </TouchableOpacity>
          <Text
            style={{
              color: 'white',
              fontSize: 25,
              fontWeight: 'bold',
              marginTop: 25,
              marginLeft: 25,
            }}>
            Total Appraisal
          </Text>
          <TextInput
            placeholder="Telusuri..."
            style={{
              width: '70%',
              marginTop: 100,
              marginLeft: -220,
              borderRadius: 8,
              backgroundColor: '#FFFFFF',
              height: 51,
              fontSize: 20,
              paddingLeft: 20,
            }}
            onChangeText={Text}
          />
          <TouchableOpacity>
            <Icon
              name="search1"
              size={25}
              color={'black'}
              style={{
                marginLeft: -45,
                marginTop: 113,
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginTop: 100,
              width: '15%',
              borderRadius: 8,
              backgroundColor: '#fff',
              height: 50,
              marginLeft: 20,
            }}>
            <Iconi
              name="tune"
              size={32}
              color={'#287AE5'}
              style={{
                marginLeft: 12,
                marginTop: 8,
              }}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '95%',
            height: 160,
            backgroundColor: 'white',
            padding: 12,
            marginHorizontal: 10,
            marginTop: 15,
            borderRadius: 7,
            borderWidth: 0.5,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '600',
              color: 'gray',
            }}>
            TR-092023-339
          </Text>
          <Text
            style={{
              width: '100%',
              marginLeft: 170,
              fontSize: 15,
              fontWeight: '600',
              color: '#002558',
              marginTop: -21,
            }}>
            Sen, 10 Juni 2023 - 10:10
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: '#002558',
              marginTop: 15,
              fontWeight: 'bold',
            }}>
            Avanza G 2.0
          </Text>
          <View
            style={{
              borderStyle: 'dashed',
              borderWidth: 1,
              marginTop: 12,
              borderColor: 'gray',
            }}></View>
          <Text
            style={{
              fontSize: 15,
              color: '#002558',
              marginTop: 20,
              fontWeight: 'bold',
            }}>
            Handoko
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: '#002558',
              marginTop: 5,
            }}>
            Cabang Kepala Gading
          </Text>
          <View
            style={{
              width: '65%',
              height: '18%',
              backgroundColor: '#287AE5',
              marginTop: -20,
              borderRadius: 5,
              marginLeft: 130,
            }}>
            <Text
              style={{
                color: 'white',
                marginLeft: 10,
                justifyContent: 'center',
                marginTop: 1,
                fontSize: 14,
              }}>
              Request Diskon Sudah Terupdate
            </Text>
          </View>
        </View>
        <View
          style={{
            width: '95%',
            height: 160,
            backgroundColor: 'white',
            padding: 12,
            marginHorizontal: 10,
            marginTop: 15,
            borderRadius: 7,
            borderWidth: 0.5,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '600',
              color: 'gray',
            }}>
            TR-092023-339
          </Text>
          <Text
            style={{
              width: '100%',
              marginLeft: 170,
              fontSize: 15,
              fontWeight: '600',
              color: '#002558',
              marginTop: -21,
            }}>
            Sen, 10 Juni 2023 - 10:10
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: '#002558',
              marginTop: 15,
              fontWeight: 'bold',
            }}>
            Avanza G 2.0
          </Text>
          <View
            style={{
              borderStyle: 'dashed',
              borderWidth: 1,
              marginTop: 12,
              borderColor: 'gray',
            }}></View>
          <Text
            style={{
              fontSize: 15,
              color: '#002558',
              marginTop: 20,
              fontWeight: 'bold',
            }}>
            Handoko
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: '#002558',
              marginTop: 5,
            }}>
            Cabang Kepala Gading
          </Text>
          <View
            style={{
              width: '65%',
              height: '18%',
              backgroundColor: '#A4D166',
              marginTop: -20,
              borderRadius: 5,
              marginLeft: 130,
            }}>
            <Text
              style={{
                color: 'white',
                marginLeft: 10,
                justifyContent: 'center',
                marginTop: 1,
                fontSize: 14,
              }}>
              Request Subsidi Sudah Terupdate
            </Text>
          </View>
        </View>
        <View
          style={{
            width: '95%',
            height: 160,
            backgroundColor: 'white',
            padding: 12,
            marginHorizontal: 10,
            marginTop: 15,
            borderRadius: 7,
            borderWidth: 0.5,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '600',
              color: 'gray',
            }}>
            TR-092023-339
          </Text>
          <Text
            style={{
              width: '100%',
              marginLeft: 170,
              fontSize: 15,
              fontWeight: '600',
              color: '#002558',
              marginTop: -21,
            }}>
            Sen, 10 Juni 2023 - 10:10
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: '#002558',
              marginTop: 15,
              fontWeight: 'bold',
            }}>
            Avanza G 2.0
          </Text>
          <View
            style={{
              borderStyle: 'dashed',
              borderWidth: 1,
              marginTop: 12,
              borderColor: 'gray',
            }}></View>
          <Text
            style={{
              fontSize: 15,
              color: '#002558',
              marginTop: 20,
              fontWeight: 'bold',
            }}>
            Handoko
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: '#002558',
              marginTop: 5,
            }}>
            Cabang Kepala Gading
          </Text>
          <View
            style={{
              width: '62%',
              height: '18%',
              backgroundColor: '#FFCF32',
              marginTop: -20,
              borderRadius: 5,
              marginLeft: 141,
            }}>
            <Text
              style={{
                color: 'white',
                marginLeft: 10,
                justifyContent: 'center',
                marginTop: 1,
                fontSize: 14,
              }}>
              Request MRP Sudah Terupdate
            </Text>
          </View>
        </View>
        <View
          style={{
            width: '95%',
            height: 160,
            backgroundColor: 'white',
            padding: 12,
            marginHorizontal: 10,
            marginTop: 15,
            borderRadius: 7,
            borderWidth: 0.5,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '600',
              color: 'gray',
            }}>
            TR-092023-339
          </Text>
          <Text
            style={{
              width: '100%',
              marginLeft: 170,
              fontSize: 15,
              fontWeight: '600',
              color: '#002558',
              marginTop: -21,
            }}>
            Sen, 10 Juni 2023 - 10:10
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: '#002558',
              marginTop: 15,
              fontWeight: 'bold',
            }}>
            Avanza G 2.0
          </Text>
          <View
            style={{
              borderStyle: 'dashed',
              borderWidth: 1,
              marginTop: 12,
              borderColor: 'gray',
            }}></View>
          <Text
            style={{
              fontSize: 15,
              color: '#002558',
              marginTop: 20,
              fontWeight: 'bold',
            }}>
            Handoko
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: '#002558',
              marginTop: 5,
            }}>
            Cabang Kepala Gading
          </Text>
          <View
            style={{
              width: '50%',
              height: '18%',
              backgroundColor: '#002558',
              marginTop: -20,
              borderRadius: 5,
              marginLeft: 182,
            }}>
            <Text
              style={{
                color: 'white',
                marginLeft: 10,
                justifyContent: 'center',
                marginTop: 1,
                fontSize: 14,
              }}>
              Sedang Dalam Request
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default TotalAppraiserComponent;