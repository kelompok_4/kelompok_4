import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Dimensions,
  Modal,
  ToastAndroid,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Iconiza from 'react-native-vector-icons/FontAwesome5';
import ModalTracking from '../../../modal/Tracking/ModalTracking';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ModalBottom from '../../../modal/ModalBottom';
import ModalCenter from '../../../modal/ModalCenter';
import ModalCenter2 from '../../../modal/ModalCenter2';

const BeliMobilComponent = ({ navigation, route }) => {
  const SEARCH_WIDTH = Dimensions.get('window').width
  const SEARCH_HEIGHT = Dimensions.get('window').height

  //SEND REQUEST > alert
  const [requestDiskon, setRequestDiskon] = useState('')
  const showToast = () => { ToastAndroid.show('Request Berhasil', ToastAndroid.SHORT); };

  //Request discont > modal bottom
  const [modalTracking, setModalTracking] = useState('')
  const handlePress = () => {
    setRequestDiskon(true);
    showToast();
  };

  //No Deal button > Alasan penolakan modal center
  const [modalTracking2, setModalTracking2] = useState('')
  const [show, setShow] = useState(null);
  const [opsi, setOpsi] = useState(null);
  //No Deat button - Send Request > Data Terkirim
  const [modalTracking3, setModalTracking3] = useState('')

  //Deal > Kirim
  const [modalTracking4, setModalTracking4] = useState('')

  return (
    <View style={{ flex: 1, backgroundColor: '#F7F7F7' }}>
      <View style={{ width: SEARCH_WIDTH, height: SEARCH_HEIGHT }}>
        <View style={{ backgroundColor: 'white' }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 16, }}>
            <Text style={styles.text2}>TR-092018-246</Text>
            <Text style={styles.text4}>Sen, 17 Sep 2018 - 10:30</Text>
          </View>
          <View style={styles.line} />
          <View style={{ padding: 12 }}>
            <View style={styles.view3}>
              <Text style={styles.text2}>Nama Customer</Text>
              <Text style={styles.text}>NANDO DWIKI SATRIA</Text>
            </View>
            <View style={styles.view3}>
              <Text style={styles.text2}>No.Hp</Text>
              <Text style={styles.text}>082245884655</Text>
            </View>
          </View>
          <View style={styles.line2} />
          <View style={styles.view3}>
            <Text style={styles.text}>Detail Mobil</Text>
          </View>
          <View style={styles.line} />
          <View style={styles.view3}>
            <View>
              <Text>New Car</Text>
              <Text style={styles.text}>Rush S AT TRD</Text>
            </View>
            <Text style={styles.text}>Rp 276.600.000</Text>
          </View>
          <View style={styles.line2} />
          <View style={styles.view3}>
            <Text style={styles.text}>Diskon</Text>
          </View>
          <View style={styles.line} />
          {requestDiskon === true ?
            <View style={{ backgroundColor: 'white' }}>
              <View style={styles.view3}>
                <Text style={styles.text}>Request Disc.</Text>
                <Text style={styles.text}>Rp 15.000.000</Text>
              </View>
              <View style={styles.view3}>
                <Text style={styles.text}>Request Disc.</Text>
                <Text style={styles.text}>Rp 235.000.000</Text>
              </View>
              <View style={styles.view4}>
                <Image
                  style={{ height: 20, width: 20 }}
                  source={require('../../../../asset/icon/Home/Icon.png')}
                />
                <Text style={styles.text6}>Permintaan sedang dalam proses</Text>
              </View>
            </View>
            :
            <TouchableOpacity onPress={() => setModalTracking(true)}
              style={styles.touchableopacity2}>
              <Text style={styles.text3}>REQUEST DISCOUNT</Text>
            </TouchableOpacity>
          }
        </View>
      </View>
      <View style={styles.touchableopacity}>
        <View style={{ width: '49%' }}>
          <TouchableOpacity onPress={() => setModalTracking2(true)}>
            <Text style={styles.text5}>NO DEAL</Text>
          </TouchableOpacity>
        </View>
        <View style={{ width: '2%' }}></View>
        <View style={{ width: '49%' }}>
          <TouchableOpacity onPress={() => setModalTracking4(true)}>
            <Text style={styles.text3}>DEAL</Text>
          </TouchableOpacity>
        </View>
      </View>
      <ModalBottom
        show={modalTracking}
        onClose={() => setModalTracking(false)}
        title='Request Discount' >
        <View style={{ padding: 10 }}>
          <View style={styles.modalview}>
            <Text >Masukkan Diskon</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text >OTR</Text>
              <Text style={{ fontWeight: 'bold' }}> : Rp 276.600.000</Text>
            </View>
          </View>
          <View style={styles.modaltextinput}>
          <Text>Rp.</Text>
            <TextInput placeholder='15.000.000'
              keyboardType="numeric"
            />
          </View>
          <Text>* Request Pengajuan Discount</Text>
          <View style={styles.modalview2}>
            <Text style={{ fontWeight: 'bold' }}>Notes</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text >0</Text>
              <Text >/100</Text>
            </View>
          </View>
          <TextInput placeholder='Masukkan Catatan'
            style={styles.modaltextinput2}
            multiline />
        </View>
        <TouchableOpacity onPress={handlePress}>
          <View style={{ width: '100%', padding: 10 }}>
            <Text style={styles.modaltext}>SEND REQUEST</Text>
          </View>
        </TouchableOpacity>
      </ModalBottom>
      <ModalCenter show={modalTracking2}
        onClose={() => setModalTracking2(false)}
        title='Alasan Penolakan' >
        <View style={{ padding: 10 }}>
          <Text style={styles.modalcentertext}>* Request Pengajuan Discount</Text>
          <Text style={styles.modalcentertext2}>Apakah customer tetap membeli mobil di AUTO2000?</Text>
          <View style={styles.modalcenterview2}>
            <TouchableOpacity style={{ width: '30%' }}
              onPress={() => setOpsi(opsi === 1 ? null : 1)}>
              <View style={{ flexDirection: 'row' }}>
                <FontAwesome name={opsi === null ? 'circle' : 'circle-o'}
                  size={17}
                  color={'blue'} />
                <Text style={styles.modalcentertext3}>Ya</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setOpsi(opsi === null ? 1 : null)}>
              <View style={{ flexDirection: 'row' }}>
                <FontAwesome
                  name={opsi === null ? 'circle-o' : 'circle'}
                  size={17}
                  color={'blue'} />
                <Text style={styles.modalcentertext4}>Tidak</Text>
              </View>
            </TouchableOpacity>
          </View>
          <Text style={{ marginVertical: 4 }}>Alasan Tidak Deal</Text>
          <View>
            <TouchableOpacity onPress={() => setShow(show === 2 ? null : 2)}
              style={styles.modalcentertouch}>
              <View style={{}}>
                <Text style={styles.modalcentertext5}>Pilih Alasan</Text>
              </View>
              <View>
                <AntDesign
                  name={show === 2 ? 'up' : 'down'}
                  size={17}
                  color={'black'} />
              </View>
            </TouchableOpacity>
            {show === 2 ? (
              <View style={styles.modalcenterview3}>
                <TouchableOpacity>
                  <Text
                    style={styles.modalcentertext5}>
                    hahahh
                  </Text>
                </TouchableOpacity>
              </View>) : null}
          </View>
          <View style={styles.modalview2}>
            <Text style={{ fontWeight: 'bold' }}>Notes</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text>0</Text>
              <Text>/100</Text>
            </View>
          </View>
          <TextInput placeholder='Masukkan Catatan'
            style={styles.modaltextinput2}
            multiline />
        </View>
        <TouchableOpacity onPress={() => setModalTracking3(true)}>
          <View style={{ width: '100%', padding: 10 }}>
            <Text style={styles.modaltext}>SEND REQUEST</Text>
          </View>
        </TouchableOpacity>
      </ModalCenter>
      <ModalCenter2 show={modalTracking3}
        onClose={() => setModalTracking3(false)}>
        <View style={{
          backgroundColor: 'white', marginHorizontal: 40, paddingHorizontal: 16, paddingVertical: 8
          , paddingTop: 20
        }}>
          <Image
            style={{ height: 60, width: 60, alignSelf: 'center' }}
            source={require('../../../../asset/image/Home/success.png')}
          />

          <Text style={{ color: '#002558', fontWeight: "bold", textAlign: 'center', marginVertical: 14 }}>Data Terkirim</Text>
          <Text style={{ textAlign: 'center', marginBottom: 14 }}>Data Berhasil Terupdate dan Terkirim</Text>
        </View>
      </ModalCenter2 >
      <ModalCenter show={modalTracking4}
        onClose={() => setModalTracking4(false)}
        title='Harga Final' >
        <View style={{ padding: 10 }}>
          <View
            style={styles.modalcenter3view}>
            <Text style={styles.modalcenter3text}>Masukkan Nominal</Text>
            <View style={styles.modalcenter3view3}>
              <View style={styles.modalcenter3view4}>
                <Text style={styles.modalcenter3text2}>Harga Final : </Text>
                <Text style={styles.modalcenter3text3}>Rp 155.000.000</Text>
              </View>
            </View>
          </View>
          <TextInput placeholder='Rp.'
            style={styles.modaltextinput3}
            multiline />
          <View style={styles.modalview2}>
            <Text style={{ fontWeight: 'bold' }}>Notes</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text>0</Text>
              <Text>/100</Text>
            </View>
          </View>
          <TextInput placeholder='Masukkan Catatan'
            style={styles.modaltextinput2}
            multiline />
        </View>
        <TouchableOpacity onPress={() => setModalTracking3(true)}>
          <View style={{ width: '100%', padding: 10 }}>
            <Text style={styles.modaltext}>KIRIM</Text>
          </View>
        </TouchableOpacity>
      </ModalCenter>
    </View >
  );
};

const styles = StyleSheet.create({
  textinput: {
    margin: 10,
    borderColor: '#E5E8EE',
    borderWidth: 1,
    width: '100%',
    alignSelf: 'center',
    color: '#002558',
    paddingHorizontal: 12,
  },
  view2: {
    margin: 10,
    borderColor: '#E5E8EE',
    borderWidth: 1,
    width: '100%',
    alignSelf: 'center',
    color: '#002558',
    paddingHorizontal: 12,
    flexDirection: 'row',
  },
  view3: {
    flexDirection: 'row',
    marginVertical: 6,
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  view4: {
    flexDirection: 'row',
    backgroundColor: '#F6F7F9',
    marginHorizontal: 14,
    marginVertical: 5,
    padding: 8
  },
  text: {
    color: '#002558',
    fontWeight: '700',
  },
  text2: { color: '#002558', },
  line: {
    backgroundColor: '#F7F7F7',
    width: '100%',
    height: 3,
    marginVertical: 5
  },
  line2: {
    backgroundColor: '#F7F7F7',
    width: '100%',
    height: 10,
    marginVertical: 5
  },
  text3: {
    backgroundColor: '#287AE5',
    paddingVertical: 9,
    borderRadius: 10,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
  },
  text4: {
    color: '#002558',
    fontWeight: 'bold',
  },
  text5: {
    backgroundColor: '#CCD3DD',
    paddingVertical: 9,
    borderRadius: 10,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
  },
  text6: {
    color: '#287AE5',
    marginLeft: 5
  },
  touchableopacity: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    paddingBottom: 20,
    paddingHorizontal: 20,
    width: '100%',
    flexDirection: "row",
    justifyContent: 'space-between'
  },
  touchableopacity2: {
    paddingBottom: 20,
    paddingHorizontal: 20,
    width: '100%',
  },
  modalview: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10
  },
  modalview2: {
    flexDirection: 'row',
    marginTop: 12,
    justifyContent: 'space-between'
  },
  modaltextinput: {
    paddingHorizontal: 12,
    borderColor: '#D6D6D6',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection:'row',
    alignItems:'center'
  },
  modaltextinput2: {
    marginTop: 8,
    paddingHorizontal: 12,
    paddingBottom: '5%',
    paddingVertical: 9,
    borderColor: '#D6D6D6',
    borderRadius: 10,
    borderWidth: 1,
    textAlignVertical: 'top'
  },
  modaltextinput3: {
    marginVertical: 8,
    paddingHorizontal: 12,
    paddingVertical: 9,
    borderColor: '#D6D6D6',
    borderRadius: 10,
    borderWidth: 1,
    textAlignVertical: 'top'
  },
  modaltext: {
    backgroundColor: '#287AE5',
    fontWeight: 'bold',
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
    padding: 5,
    borderRadius: 8
  },
  modalcentertext: { marginVertical: 4 },
  modalcentertext2: { marginVertical: 4, marginBottom: 10, color: 'black' },
  modalcentertext3: { color: 'black', marginLeft: 10 },
  modalcentertext4: { color: 'black', marginLeft: 10 },
  modalcentertext5: {
    color: 'black',
    fontWeight: '300',
  },
  modalcenterview2: { marginVertical: 4, flexDirection: 'row' },
  modalcenterview3: {
    backgroundColor: '#fff',
    elevation: 9,
    padding: 10,

  },
  modalcentertouch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#D6D6D6',
    backgroundColor: 'white',
    paddingVertical: 7,
    paddingHorizontal: 8,
  },
  modalcenter3view: {
    flexDirection: 'row',
    width: '100%',
    marginVertical: 8,
    justifyContent: 'space-between'
  },
  modalcenter3text: {
    color: 'black',
    width: '40%'
  },
  modalcenter3text2: {
    wordWrap: 'break-word',
    width: '50%'
  },
  modalcenter3text3: {
    fontWeight: 'bold',
    wordWrap: 'break-word',
    width: '50%',
    textAlign: 'right'
  },
  modalcenter3view3: {
    flexDirection: 'row-reverse',
    width: '60%'
  },
  modalcenter3view4: {
    flexDirection: 'row',
    wordWrap: 'break-word',
    width: '100%'
  },
})
export default BeliMobilComponent;


