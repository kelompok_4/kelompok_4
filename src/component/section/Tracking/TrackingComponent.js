import React, { useState } from 'react';
import { Dimensions, TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,

} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import ModalTracking from '../../modal/Tracking/ModalTracking';
import { TextBold } from '../../global';
import { Colors } from '../../../styles';

const TrackingComponent = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width - 93

    const [searching, setSearching] = useState('')

    const [modalTracking, setModalTracking] = useState('')

    const [available, setAvailable] = useState('');
    const handleOpen = () => {
        setAvailable('');
    };
    const handleClose = () => {
        setAvailable('Close');
    };

    return (
        <View>
            <View>
                <View style={{ backgroundColor: '#287AE5', height: 135, padding: 15 }}>
                    <TextBold
                        text="Tracking"
                        color={Colors.WHITE}
                        size={20}
                        style={{ marginBottom: 15 }}
                    />
                    <View style={{
                        flexDirection: 'row',
                        height: 48,
                        width: SEARCH_WIDTH,
                    }}>
                        <View style={{
                            backgroundColor: '#FFFFFF',
                            height: 48,
                            flexDirection: 'row',
                            width: '100%',
                            justifyContent: 'space-between',
                            borderRadius: 8,
                            paddingHorizontal: 5
                        }}>
                            <TextInput placeholder='Telusuri....'
                                onChangeText={(text) => setSearching(text)}
                                value={searching}></TextInput>
                            <TouchableOpacity onPress={() => ''}
                                style={{ alignSelf: 'center' }}>
                                <AntDesign name="search1" size={30} color="black" />
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => setModalTracking(true)}>
                            <MaterialCommunityIcons name="tune" size={25} color="black"
                                style={{
                                    backgroundColor: '#FFFFFF',
                                    borderRadius: 8,
                                    padding: 12,
                                    marginLeft: 12,
                                }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={{
                backgroundColor: '#FFFFFF',
                paddingHorizontal: 12,
                borderRadius: 8,
                marginTop: '-4%',
                flexDirection: 'row',
                alignSelf: 'center',
                marginHorizontal: 16,
            }}>
                <TouchableOpacity onPress={() => handleOpen()}
                    style={styles.buttonTrade}>
                    {available === '' ?
                        <Text style={{ color: '#287AE5' }}>Trade In</Text>
                        :
                        available === 'Close' ?
                            <Text style={{ color: 'grey' }}>Trade In</Text>
                            : ""}
                </TouchableOpacity>
                <View style={{
                    borderColor: 'rgba(0,0,0,0.1)',
                    borderWidth: 1,
                    height: '100%',

                }} />
                <TouchableOpacity onPress={() => handleClose()}
                    style={styles.buttonTrade}>
                    {available === '' ?
                        <Text style={{ color: 'grey' }}>New Car</Text>
                        :
                        available === 'Close' ?
                            <Text style={{ color: '#287AE5' }}>New Car</Text>
                            : ""}
                </TouchableOpacity>
            </View>
            {
                available === '' ?
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic"
                        style={{ paddingHorizontal: 16, paddingTop: 12 }} >
                        <TouchableOpacity onPress={() => navigation.navigate('ToolsTradeIn')}
                            style={{
                                backgroundColor: 'white',
                                height: 'auto',
                                width: '100%',
                                borderRadius: 8,
                                padding: 12,
                                borderColor: 'rgba(0,0,0,0.2)',
                                borderWidth: 1,
                            }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.idNewCar}>TR-092018-246</Text>
                                <Text style={styles.tanggal}>Sen, 17 Sep 2018 - 10:30</Text>
                            </View>
                            <Text style={styles.namaMobil}>Avanza G 2.0</Text>
                            <View style={styles.garisPembatas}></View>
                            <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View>
                                    <Text style={styles.namaPekerja}>Handoko</Text>
                                    <Text style={{
                                        fontSize: 10,
                                        color: 'rgba(0, 37, 88, 0.5)',
                                    }}>Salesman</Text>
                                </View>
                                <View style={{ flexDirection: 'row-reverse' }}>
                                    {/* <Text style={{
                                        backgroundColor: '#002558',
                                        color: '#FFFFFF',
                                        fontSize: 10,
                                        borderRadius: 10,
                                        padding: 5,
                                        alignSelf: 'center',
                                        textAlign: 'right',
                                    }}
                                    >Sedang Dalam Request</Text> */}
                                    {/* <Text style={{
                                        backgroundColor: '#FFCF32',
                                        textAlign: 'right',
                                        color: '#FFFFFF',
                                        fontSize: 10,
                                        borderRadius: 10,
                                        padding: 5,
                                        alignSelf: 'center'
                                    }}
                                    >Request MRP Sudah Terupdate</Text> */}
                                    {/* <Text style={{
                                        backgroundColor: '#A4D166',
                                        textAlign: 'right',
                                        color: '#FFFFFF',
                                        fontSize: 10,
                                        borderRadius: 10,
                                        padding: 5,
                                        alignSelf: 'center'
                                    }}
                                    >Request Subsudi Sudah Terupdate</Text> */}
                                    <Text style={{
                                        backgroundColor: '#287AE5',
                                        textAlign: 'right',
                                        color: '#FFFFFF',
                                        fontSize: 10,
                                        borderRadius: 10,
                                        padding: 5,
                                        alignSelf: 'center'
                                    }}
                                    >Request Diskon Sudah Terupdate</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </ScrollView >
                    :
                    available === 'Close' ?
                        <ScrollView
                            contentInsetAdjustmentBehavior="automatic"
                            style={{ position: 'relative', paddingHorizontal: 16, paddingTop: 12 }} >
                            <TouchableOpacity onPress={() => navigation.navigate('BeliMobil')}
                                style={{
                                    backgroundColor: 'white',
                                    height: 'auto',
                                    width: '100%',
                                    borderRadius: 8,
                                    padding: 12,
                                    borderColor: 'rgba(0,0,0,0.2)',
                                    borderWidth: 1,
                                }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.idNewCar}>TR-092018-246</Text>
                                    <Text style={styles.tanggal}>Sen, 17 Sep 2018 - 10:30</Text>
                                </View>
                                <Text style={styles.namaMobil}>Avanza G 2.0</Text>
                                <View style={styles.garisPembatas}></View>
                                <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ width: '30%' }}>
                                        <Text style={styles.namaPekerja}>Handoko</Text>
                                        <Text style={styles.pekerjaan}>Salesman</Text>
                                    </View>
                                    <View style={{ width: '100%' }}>
                                        <Text style={styles.dalamRequest}
                                        >Sedang Dalam Request</Text>
                                        {/* <Text style={styles.sudahRequest}
                        >Request Diskon Sudah Terupdate</Text> */}
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </ScrollView >
                        : ""
            }
            <ModalTracking
                show={modalTracking}
                onClose={() => setModalTracking(false)}
                title='Filter' >
                <View style={{ padding: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, borderBottomWidth: 0.5, borderBottomColor: '#ccc' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>Request Diskon</Text>
                        <FontAwesome name="circle-o" size={16} color="#002558" />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>Request Subsidi</Text>
                        <FontAwesome name="dot-circle-o" size={16} color="#002558" />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#002558' }}>Request MRP</Text>
                        <FontAwesome name="dot-circle-o" size={16} color="#002558" />
                    </View>
                    <View style={{ width: '100%', padding: 10 }}>
                        <Text style={{
                            backgroundColor: '#287AE5',
                            fontWeight: 'bold',
                            fontSize: 16,
                            color: 'white',
                            textAlign: 'center',
                            padding: 5,
                            borderRadius: 8
                        }}>SIMPAN</Text>
                    </View>
                </View>
            </ModalTracking>
        </View >
    );
};

const styles = StyleSheet.create({
    buttonTrade: {
        paddingVertical: 9,
        width: '50%',
        alignItems: 'center',
        borderColor: '#0000000F',
    },
    idNewCar: {
        fontSize: 12,
        textAlign: 'left',
        color: 'rgba(0, 37, 88, 0.5)',
    },
    tanggal: {
        fontSize: 12,
        color: '#002558',
        right: 0,
        position: 'absolute'
    },
    namaMobil: {
        fontSize: 14,
        marginTop: 8,
        color: '#002558',
        fontWeight: 'bold'
    },
    garisPembatas: {
        marginTop: 10,
        borderStyle: 'dashed',
        borderWidth: 1,
        borderRadius: 1,
        borderColor: 'rgba(0,0,0,0.3)',
    },
    namaPekerja: {
        fontSize: 12,
        color: '#002558',
        fontWeight: 'bold',
    },
    pekerjaan: {
        fontSize: 10,
        color: 'rgba(0, 37, 88, 0.5)',
    },
    dalamRequest: {
        backgroundColor: '#002558',
        textAlign: 'right',
        color: '#FFFFFF',
        fontSize: 10,
        borderRadius: 10,
        padding: 5,
        alignSelf: 'center'
    },
    sudahRequest: {
        backgroundColor: '#FF6127',
        textAlign: 'right',
        color: '#FFFFFF',
        fontSize: 10,
        borderRadius: 10,
        padding: 5,
        alignSelf: 'center'
    }

})


export default TrackingComponent;
