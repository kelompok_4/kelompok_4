import React, { useState } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    StyleSheet
} from 'react-native'
import { TextRegular, TextBold, TextMedium, Header, InputText } from '../../../global'
import Icon from 'react-native-vector-icons/FontAwesome'
import ModalCenter from '../../../modal/ModalCenter'

const SummaryTradeInComponent = ({
    navigation,
    route,
    deal,
    setDeal,
    modalOdoo,
    setModalOdoo,
    modalError,
    setModalError }) => {

    return (
        <View style={{ flex: 1 }}>
            <ScrollView>
                <View style={styles.headerView}>
                    <Text style={styles.headerText}>Deal</Text>
                    <Text style={styles.headerText}>19 Agutus 2020 - 15.43</Text>
                </View>
                <View style={styles.coloumView}>
                    <Text style={{ color: 'rgba(0, 0, 0, 0.3)', fontSize: 12 }}>TR-092018-246</Text>
                    <Text style={styles.text}>Kepala Cabang Bintaro</Text>
                </View>
                <View style={styles.coloumViewMultiple}>
                    <View style={styles.coloumView2}>
                        <Text style={styles.text}>Nama Customer</Text>
                        <Text style={styles.textBold}>NANDO DWIKI SATRIA</Text>
                    </View>
                    <View style={styles.coloumView2}>
                        <Text style={styles.text}>No.Hp</Text>
                        <Text style={styles.text}>082245884655</Text>
                    </View>
                </View>
                <View style={styles.coloumViewMultiple}>
                    <View style={styles.coloumView2}>
                        <Text style={styles.textBold}>Detail Mobil</Text>
                    </View>
                    <View style={styles.underLine} />
                    <View >
                        <Text style={styles.title}>Used Car</Text>
                    </View>
                    <View style={styles.coloumView4}>
                        <Text style={styles.textBold}>AVANZA G AT 2016</Text>
                        <Text style={styles.textBold}>Rp 81.500.000</Text>
                    </View>
                    <View >
                        <Text style={styles.title}>New Car</Text>
                    </View>
                    <View style={styles.coloumView4}>
                        <Text style={styles.textBold}>Rush S AT TRD</Text>
                        <Text style={styles.textBold}>Rp 276.600.000</Text>
                    </View>
                </View>
                <View style={styles.coloumViewMultiple}>
                    <View style={styles.coloumView2}>
                        <Text style={styles.textBold}>Request Diskon</Text>
                    </View>
                    <View style={styles.underLine} />
                    <View style={styles.coloumView5}>
                        <Text style={styles.text}>Diskon</Text>
                        <Text style={styles.textBold}>Rp 9.000.000</Text>
                    </View>
                    <View style={styles.coloumView5}>
                        <Text style={styles.text}>Proyeksi GP (After Disc)</Text>
                        <Text style={styles.textBold}>Rp 3.000.000</Text>
                    </View>
                </View>
                <View style={styles.coloumViewMultiple}>
                    <View style={styles.coloumView2}>
                        <Text style={styles.textBold}>Request MRP</Text>
                    </View>
                    <View style={styles.coloumView4}>
                        <Text style={styles.text}>Nominal Request</Text>
                        <Text style={styles.textBold}>Rp 156.187.500</Text>
                    </View>
                    <View style={styles.coloumView4}>
                        <Text style={styles.text}>Ruang Nego</Text>
                        <Text style={{
                            color: 'green',
                            fontSize: 12,
                            fontWeight: "bold",
                        }}>+ Rp 4.685.625</Text>
                    </View>
                </View>

            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#A4D166',
        height: 36,
        alignItems: 'center',
        paddingHorizontal: 16
    },
    headerText: {
        color: '#FFFFFF',
        fontSize: 12
    },
    coloumView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        padding: 15,
        borderColor: 'rgba(0,0,0,0.2)',
        borderWidth: 1,
    },
    coloumView2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        paddingVertical: 15,
    },
    coloumView3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        padding: 15,
    },
    coloumView4: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: 'white',
        paddingBottom: 15,
    },
    coloumView5: {
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: 'white',
        paddingBottom: 15,
    },
    coloumViewMultiple: {
        borderColor: 'rgba(0,0,0,0.2)',
        borderWidth: 1,
        paddingHorizontal: 15,
    },
    text: {
        color: '#002558',
        fontSize: 12
    },
    textBold: {
        color: '#002558',
        fontSize: 12,
        fontWeight: 'bold',
    },
    underLine: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.3)',
        borderStyle: 'solid',
        borderRadius: 1,
    },
    title:{
        color: 'rgba(0,0,0,0.5)',
        fontSize: 12,
    },
})

export default SummaryTradeInComponent;