import React, { useState } from 'react';
import { Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    Image,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const DataCustomerComponent = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width
    const SEARCH_HEIGHT = Dimensions.get('window').height

    return (
        <View style={styles.box} >
            <Text style={styles.title}>Data Customer</Text>
            <View style={styles.line} />
            <View style={styles.view}>
                <Text style={styles.text}>Pemilik Kendaraan</Text>
                <Text style={styles.text2}>Budi Sarudi</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>No. HP Customer</Text>
                <Text style={styles.text2}>085212457896</Text>
            </View>
            <View style={styles.view}>
                <Text style={styles.text}>E-mail Customer</Text>
                <Text style={styles.text2}>budi@gmail.com</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    viewIconTop: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconTop: {
        height: 50,
        resizeMode: 'contain'
    },
    kondisi: {
        backgroundColor: '#EBFDF5',
        paddingVertical: 12,
        marginHorizontal: 12,
        color: '#1B385D',
        fontWeight: 'bold',
        marginTop: 12,
        textAlign: 'center'
    },
    title: {
        color: '#1B385D',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
    },
    box: {
        borderRadius: 8,
        backgroundColor: 'white',
        marginTop: 15,
        paddingTop: 12,
        marginBottom: 15,
        paddingBottom: 12,
    },
    line: {
        marginVertical: 10,
        borderColor: '#F2F6FA',
        height: 1,
        borderWidth: 1,
    },
    view: {
        marginTop: 13,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 12
    },
    text: {
        color: '#005596',
        fontSize: 12
    },
    text2: {
        color: '#1B385D',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    text3: {
        color: '#E5394B',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    icon: {
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    iconText: {
        color: '#1B385D',
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10,
    },
    viewIcon: {
        flexDirection: 'row',
        paddingHorizontal: 12,
        marginTop: 26,
    },
})


export default DataCustomerComponent ;
