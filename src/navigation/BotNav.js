import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome'
import IconM from 'react-native-vector-icons/MaterialCommunityIcons'
import { Image } from 'react-native';


import Home from '../screen/Home/Home';
import Tracking from '../screen/Tracking/Tracking';
import Riwayat from '../screen/Riwayat/Riwayat';
import Insentif from '../screen/Insentif/Insentif';
import Profil from '../screen/Profil/Profile';


const Tab = createBottomTabNavigator();
function BotNav() {
  return (
    <Tab.Navigator screenOptions={{headerShown: false}} initialRouteName='Home'>

      <Tab.Screen name='Home' component={Home}
      options={{
      tabBarLabel: 'Home',
      tabBarActiveTintColor: '#002558',
      tabBarInactiveTintColor: '#D8D8D8',
      tabBarShowLabel: true,
      tabBarIcon: ({focused}) => (<Image source={require('../asset/icon/botnav/home.png')} style={{tintColor:focused?'#002558':'#D8D8D8',width:20,height:20}}/>),
      }}/>

      <Tab.Screen name='Tracking' component={Tracking}
      options={{
      tabBarLabel: 'Tracking',
      tabBarActiveTintColor: '#002558',
      tabBarInactiveTintColor: '#D8D8D8',
      tabBarShowLabel: true,
      tabBarIcon: ({focused}) => (<Image source={require('../asset/icon/botnav/tracking.png')} style={{tintColor:focused?'#002558':'#D8D8D8',width:20,height:20}}/>),
      headerShown: false,
      }}/>

      <Tab.Screen name='Riwayat' component={Riwayat}
      options={{
      tabBarLabel: 'Riwayat',
      tabBarActiveTintColor: '#002558',
      tabBarInactiveTintColor: '#D8D8D8',
      tabBarShowLabel: true,
      tabBarIcon: ({focused}) => (<Image source={require('../asset/icon/botnav/riwayat.png')} style={{tintColor:focused?'#002558':'#D8D8D8',width:17,height:20}}/>),
      }}/>

    <Tab.Screen name='Insentif' component={Insentif}
      options={{
      tabBarLabel: 'Insentif',
      tabBarActiveTintColor: '#002558',
      tabBarInactiveTintColor: '#D8D8D8',
      tabBarShowLabel: true,
      tabBarIcon: ({focused}) => (<Image source={require('../asset/icon/botnav/insentif.png')} style={{tintColor:focused?'#002558':'#D8D8D8',width:22,height:22}}/>),
      }}/>

    <Tab.Screen name='Profil' component={Profil}
      options={{
      tabBarLabel: 'Profil',
      tabBarActiveTintColor: '#002558',
      tabBarInactiveTintColor: '#D8D8D8',
      tabBarShowLabel: true,
      tabBarIcon: ({focused}) => (<Image source={require('../asset/icon/botnav/profil.png')} style={{tintColor:focused?'#002558':'#D8D8D8',width:18.66,height:20}}/>),
      }}/>

    </Tab.Navigator>
  );
}

export default BotNav;
