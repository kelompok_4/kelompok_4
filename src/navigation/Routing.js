import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Icon from "react-native-vector-icons/FontAwesome";
import { TouchableOpacity, Button } from "react-native";

import AuthNavigation from "./AuthNavigation";
import SummaryTradeIn from "../screen/Riwayat/SummaryTradeIn";
import BotNav from "./BotNav";
import CekHarga from "../screen/Home/CekHarga/CekHarga";
import TotalAppraiser from "../screen/Home/Total Appraisal/TotalAppraiser";
import PO from "../screen/Home/PO/PO";
import PoValid from "../screen/Home/PO Valid/PoValid";
import DetailAppraisal from "../screen/Home/Detail Appraisal/DetailAppraisal";
import DetailAppraisal3 from "../screen/Tracking/ToolTradeIn/DetailAppraisal3/DetailAppraisal3";
import BeliMobil from "../screen/Home/Beli Mobil/BeliMobil";
import ToolsTradeIn from "../screen/Home/Tools Trade In/ToolsTradeIn";
import SummaryNewCar from "../screen/Riwayat/SummaryNewCar";
import RequestDalamProses from "../screen/Home/Request Dalam Proses/RequestDalamProses";
import CekHargaKendaraan from "../screen/Profil/ProfileCekHargaMobil/CekHargaKendaraan";
import CekHasilHarga from "../screen/Profil/ProfileCekHargaMobil/CekHasilHarga/CekHasilHarga";
import DataDiri from "../screen/Profil/ProfileDataDiri/DataDiri";
import DataRekening from "../screen/Profil/ProfileDataRekening/DataRekening";
import DataDokumen from "../screen/Profil/ProfileDataDokumen/DataDokumen";


const Stack = createNativeStackNavigator();

export default function Routing() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
                <Stack.Screen name="BotNav" component={BotNav} />

                {/* Navigasi Home */}
                <Stack.Screen name="CekHarga" component={CekHarga}  />
                <Stack.Screen name="TotalAppraiser" component={TotalAppraiser} />
                <Stack.Screen name="PO" component={PO} />
                <Stack.Screen name="PoValid" component={PoValid} />
                <Stack.Screen name="DetailAppraisal" component={DetailAppraisal} />
                <Stack.Screen name="BeliMobil" component={BeliMobil} 
                    options={{ headerShown: true, title: 'Beli Mobil', headerTitleAlign: 'center' }} />
                <Stack.Screen name="ToolsTradeIn" component={ToolsTradeIn}
                    options={{ headerShown: true, title: 'Tools Trade In', headerTitleAlign: 'center' }} />
                <Stack.Screen name="RequestDalamProses" component={RequestDalamProses} />


                {/* Navigasi Tracking */}
                <Stack.Screen name="DetailAppraisal3" component={DetailAppraisal3} />

                {/* Navigasi Riwayat */}
                <Stack.Screen name="SummaryTradeIn" component={SummaryTradeIn} options={{
                    headerShown: true, title: 'Summary Trade In', headerTitleAlign: 'center', headerRight: () => (
                        <TouchableOpacity><Icon name="share-alt" size={20} /></TouchableOpacity>
                    ),
                }} />
                <Stack.Screen name="SummaryNewCar" component={SummaryNewCar} options={{
                    headerShown: true, title: 'Summary New Car', headerTitleAlign: 'center', headerRight: () => (
                        <TouchableOpacity><Icon name="share-alt" size={20} /></TouchableOpacity>
                    ),
                }} />

                {/* Navigasi Insentif */}

                {/* Navigasi Profil */}
                <Stack.Screen name="CekHargaKendaraan" component={CekHargaKendaraan} options={{
                    headerShown: true, title: 'Cek Harga Kendaraan', headerTitleAlign: 'center', headerRight: () => (
                        <TouchableOpacity><Icon name="share-alt" size={20} /></TouchableOpacity>
                    ),
                }} />
                <Stack.Screen name="CekHasilHarga" component={CekHasilHarga} options={{
                    headerShown: true, title: 'Hasil Cek Harga', headerTitleAlign: 'center', 
                }} />
                <Stack.Screen name="DataDiri" component={DataDiri} options={{
                    headerShown: true, title: 'Data Diri', headerTitleAlign: 'center', 
                }} />
                <Stack.Screen name="DataRekening" component={DataRekening} options={{
                    headerShown: true, title: 'Data Rekening', headerTitleAlign: 'center', 
                }} />
                <Stack.Screen name="DataDokumen" component={DataDokumen} options={{
                    headerShown: true, title: 'Data Dokumen', headerTitleAlign: 'center', 
                }} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}