import React from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Dimensions,
} from 'react-native';
import CekHargaKendaraanComponent from '../../../component/section/Profile/ProfileCekHargaMobil/CekHargaKendaraanComponent';


const CekHargaKendaraan = ({ navigation, route }) => {

    return (
        <View style={{ flex: 1, backgroundColor: '#f7f7f7' }}>
            <CekHargaKendaraanComponent 
                navigation={navigation}
            />
        </View >
    );
};

const styles = StyleSheet.create({
})

export default CekHargaKendaraan;

