import React from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Dimensions,
} from 'react-native';
import CekHasilHargaComponent from '../../../../component/section/Profile/ProfileCekHargaMobil/CekHasilHarga/CekHasilHargaComponent';


const CekHasilHarga = ({ navigation, route }) => {

    return (
        <View style={{ flex: 1, backgroundColor: '#f7f7f7' }}>
            <CekHasilHargaComponent 
                navigation={navigation}
            />
        </View >
    );
};

const styles = StyleSheet.create({
})

export default CekHasilHarga;

