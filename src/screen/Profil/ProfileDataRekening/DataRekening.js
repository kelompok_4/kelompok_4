import React, {useState} from 'react';
import DataRekeningComponent from '../../../component/section/Profile/ProfileDataRekening/DataRekeningComponent';

const DataRekening = ({navigation, route}) => {
  return <DataRekeningComponent navigation={navigation} />;
};

export default DataRekening;
