import React from 'react';
import {
    View,
    Text,
    Image,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Dimensions,
} from 'react-native';
import ProfileComponent from '../../component/section/Profile/ProfileComponent';


const Profil = ({ navigation, route }) => {

    return (
        <View style={{ flex: 1, backgroundColor: '#f7f7f7' }}>
            <ProfileComponent 
                navigation={navigation}
            />
        </View >
    );
};

const styles = StyleSheet.create({
})
export default Profil;
