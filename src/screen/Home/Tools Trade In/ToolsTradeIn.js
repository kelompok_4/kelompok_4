import React, {useState} from 'react'
import ToolsTradeInComponent from '../../../component/section/Home/Tools Trade In/ToolsTradeInComponent'

const ToolsTradeIn = ({
   navigation,
   route
}) => {

   const [deal, setDeal] = useState(true)
   const [modalOdoo, setModalOdoo] = useState(false)
   const [modalError, setModalError] = useState(false)

   return(
       <ToolsTradeInComponent
       navigation = {navigation}
       deal = {deal}
       setDeal = {()=>setDeal(!deal)}
       modalOdoo={modalOdoo}
       setModalOdoo={()=>setModalOdoo(!modalOdoo)}
       modalError={modalError}
       setModalError={()=>setModalError(!modalError)}
       />
   )
}

export default ToolsTradeIn;