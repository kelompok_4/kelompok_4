import React, { PureComponent, useState } from 'react';
import { TouchableOpacity } from 'react-native';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
} from 'react-native';
import RequestDalamProsesComponent from '../../component/section/Home/RequestDalamProsesComponent.js';

const RequestDalamProses = ({ navigation, route }) => {

  return (
    <RequestDalamProsesComponent navigation={navigation}/>
  );
};



export default RequestDalamProses;
