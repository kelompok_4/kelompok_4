import React, { useState } from 'react';
import { Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    Image,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DataCustomerComponent from '../../component/section/Home/DetailAppraisalComponent/DataCustomerComponent';
import HasilPerhitunganFinal from '../../component/section/Home/DetailAppraisalComponent/HasilPerhitunganFinal';
import MobileComponent from '../../component/section/Home/DetailAppraisalComponent/MobileComponent';

const DetailAppraisal = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width

    return (
        <View style={{ flex: 1, backgroundColor: '#F2F6FA' }}>
            <ImageBackground source={require('../../asset/image/Summary/DetailAppraisal3.png')}
                style={{ height: 112, width: SEARCH_WIDTH, backgroundColor: '#F2F6FA' }} >

                <View style={{ flexDirection: 'row', width: '90%', marginHorizontal: 12, marginVertical: 17 }}>
                    <AntDesign name="arrowleft" size={30} color="white"
                        style={{ justifyContent: 'flex-start' }} />
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 'bold',
                        color: 'white',
                        justifyContent: 'center',
                        textAlign: 'center',
                        width: '100%'
                    }}>Summary</Text>
                </View>
            </ImageBackground>
            <ScrollView style={{ height: 'auto', marginHorizontal: 12, marginTop: '-15%' }}>
                <MobileComponent/>
                <HasilPerhitunganFinal/>
                <DataCustomerComponent/>
            </ScrollView>
        </View >
    );
};

const styles = StyleSheet.create({
    viewIconTop: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconTop: {
        height: 50,
        resizeMode: 'contain'
    },
    kondisi: {
        backgroundColor: '#EBFDF5',
        paddingVertical: 12,
        marginHorizontal: 12,
        color: '#1B385D',
        fontWeight: 'bold',
        marginTop: 12,
        textAlign: 'center'
    },
    title: {
        color: '#1B385D',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
    },
    box: {
        borderRadius: 8,
        backgroundColor: 'white',
        marginTop: 15,
        paddingTop: 12,
        marginBottom: 15,
        paddingBottom: 12,
    },
    line: {
        marginVertical: 10,
        borderColor: '#F2F6FA',
        height: 1,
        borderWidth: 1,
    },
    view: {
        marginTop: 13,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 12
    },
    text: {
        color: '#005596',
        fontSize: 12
    },
    text2: {
        color: '#1B385D',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    text3: {
        color: '#E5394B',
        fontSize: 12,
        fontWeight: 'bold',
        textAlign: 'right',
    },
    icon: {
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    iconText: {
        color: '#1B385D',
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10,
    },
    viewIcon: {
        flexDirection: 'row',
        paddingHorizontal: 12,
        marginTop: 26,
    },
})


export default DetailAppraisal;
