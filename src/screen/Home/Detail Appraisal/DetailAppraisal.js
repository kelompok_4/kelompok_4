import React, { useState } from 'react';
import { Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    Image,
} from 'react-native';
import DetailAppraisalComponent from '../../../component/section/Home/Detail Appraisal/DetailAppraisalComponent';


const DetailAppraisal = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width

    return (
        <DetailAppraisalComponent
        navigation={navigation}/ >
    );
};

export default DetailAppraisal;
