import React, {useState, useEffect} from 'react'
import HomeComponent from '../../component/section/Home/HomeComponent'



const Home = ({
   navigation,
}) => {

   return(
       <HomeComponent
       navigation={navigation}
       />

   )
}

export default Home;
