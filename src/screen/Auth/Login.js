import React, { useState } from 'react'
import {
   View,
   TouchableOpacity,
   StyleSheet,
   Dimensions,
   Text,
} from 'react-native'
import { TextRegular, TextBold, TextMedium, Header, InputText } from '../../component/global'
import { Colors } from '../../styles'
import LoginComponent from '../../component/section/Auth/LoginComponent'
import {useSelector, useDispatch} from  'react-redux'

const Login = ({ navigation, route }) => {

   const [email, setEmail] = useState('')
   const [password, setPassword] = useState('')
   const [hidePassword, setHidePassword] = useState(true)

   const { isLoggedIn } = useSelector((state) => state.auth);

   React.useEffect(() => {
       if(isLoggedIn) {
           navigation.replace('BotNav')
       }
   },[])

   const url = 'http://147.139.209.190:3002/api/v1/';
   const dispatch = useDispatch()
   
   const onLogin = async () => {
      const data = {
          email : email,
          password : password
      }
      try {
          const response = await fetch(`${url}user/login`, {
              method: "POST",
              headers: {
                  "Content-Type": "application/json",
              },
              body: JSON.stringify(data),
          });      
          const result = await response.json();
          console.log("Success:", result);
          if(result.code === 200) {
              const data = result
              dispatch({type: "LOGIN_SUCCESS", data})
              console.log('data yang terkirim ke reducer: ', result)
              navigation.replace('BotNav')
          }
          if(result.code === 400 ){
            alert(result.message)
            return false
          }
      } catch (error) {
          console.error("Error:", error);
          alert('Email atau Password salah!')
          return false
      }
  }

   const SEARCH_WIDTH = Dimensions.get('window').width
   const SEARCH_HEIGHT = Dimensions.get('window').height

   return (
      <View style={{ flex: 1, backgroundColor: '#F7F7F7' }}>
            <LoginComponent
               navigation={navigation}
               SEARCH_WIDTH={SEARCH_WIDTH}
               SEARCH_HEIGHT={SEARCH_HEIGHT}
               email={email}
               setEmail={(text) => setEmail(text)}
               password={password}
               setPassword={(text) => setPassword(text)}
               hidePassword={hidePassword}
               setShowPassword={() => setHidePassword(prevState => !prevState)}
               onPressLogin={() => onLogin()}
            />
      </View>
   )
}

const styles = StyleSheet.create({
   btnShowModalBottom: {
      width: '35%',
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 10,
      borderRadius: 6,
      backgroundColor: Colors.PRIMARY,
      alignSelf: 'center',
      marginTop: 20
   }
})

export default Login;
