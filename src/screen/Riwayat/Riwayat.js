import React, {useState} from 'react'
import { Dimensions } from 'react-native'
import RiwayatComponent from '../../component/section/Riwayat/RiwayatComponent'

const Riwayat = ({
   navigation,
}) => {

   const [modalBottom, setModalBottom] = useState(false)
   const [newCar, setNewCar] = useState(false)
   const [search, setSearch] = useState('')
   const SEARCH_WIDTH = Dimensions.get('window').width - 93

   return(
       <RiwayatComponent
       navigation={navigation}
       modalBottom={modalBottom}
       setModalBottom={()=>setModalBottom(!modalBottom)}
       newCar={newCar}
       setNewCar={()=>setNewCar(!newCar)}
       search={search}
       setSearch={(text)=>setSearch(text)}
       SEARCH_WIDTH={SEARCH_WIDTH}
       />
   )
}

export default Riwayat;
