import React, { useState } from 'react';
import { Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    Image,
} from 'react-native';
import DetailAppraisal3Component from '../../../../component/section/Tracking/ToolTradeIn/DetailAppraisal3/DetailAppraisal3Component';


const DetailAppraisal3 = ({ navigation, route }) => {
    const SEARCH_WIDTH = Dimensions.get('window').width

    return (
        <DetailAppraisal3Component
        navigation={navigation}/ >
    );
};

export default DetailAppraisal3;
