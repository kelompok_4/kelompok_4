import React, {useState} from 'react'
import SummaryTradeInComponent from '../../../component/section/Tracking/SummaryTradeIn/SummaryTradeInComponent'

const SummaryTradeIn = ({
   navigation,
   route
}) => {

   const [deal, setDeal] = useState(true)
   const [modalOdoo, setModalOdoo] = useState(false)
   const [modalError, setModalError] = useState(false)

   return(
       <SummaryTradeInComponent
       navigation = {navigation}
       deal = {deal}
       setDeal = {()=>setDeal(!deal)}
       modalOdoo={modalOdoo}
       setModalOdoo={()=>setModalOdoo(!modalOdoo)}
       modalError={modalError}
       setModalError={()=>setModalError(!modalError)}
       />
   )
}

export default SummaryTradeIn;