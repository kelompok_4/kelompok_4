import React, { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
} from 'react-native';
import TrackingComponent from '../../component/section/Tracking/TrackingComponent.js';

const Tracking = ({ navigation, route }) => {

  return (
    <TrackingComponent navigation={navigation}/>
  );
};



export default Tracking;
